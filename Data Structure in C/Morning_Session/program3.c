/*3.Concatination in Singly Linked List of 'N' number of nodes from first */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node* next;
}node;

node *head1;
node *head2;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the data :  ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

void addNode(node **head){
	node *newNode = createNode();

	if(*head == NULL){
		*head = newNode;
	}else{
		node *temp = *head;

		while(temp->next != NULL){
			temp= temp->next;
		}
		temp->next = newNode;
	}
}

int printLL(){
	if(head1 == NULL && head2 == NULL){
		printf("Concatination is not Possible\n");
		return -1;
	}else{
	node *temp = head1;
	
	while(temp->next != NULL){
		printf("| %d |->",temp->data);
		temp = temp->next;
	}
	printf("| %d |",temp->data);
	}
	return 0;
}

int countNode(){
	node *temp = head2;
	
	int count;

	while(temp != NULL){
		count++;
		temp =temp->next;
	}
	return count;
}


int ConcatSLL(int num){
	if(head1 == NULL || head2 == NULL ){
		printf("Please Create two linked lists First\n");
		return -1;
	}else{
		if(num<0){
			printf("Invalid Position\n");
			return -2;
		}else{

		node *temp1 = head1;

		while(temp1->next != NULL){
		temp1 = temp1->next;
		}

		node *temp2 = head2;
	
		while(num-1){
		temp1->next = temp2;	
		temp2 = temp2->next;
		temp1 = temp1->next;
		num--;
		}

		temp1->next->next = NULL;
		}
	}
	return 0;	
}

void main(){
	char choice;

	do{
		printf("~~~~~~~~~~~~~~~~~~\n");
		printf("|1. Linked List 1|\n");
		printf("|2. Linked List 2|\n");
		printf("|3. PrintSLL     |\n");
		printf("|4. ConcatSLL    |\n");
		printf("~~~~~~~~~~~~~~~~~~\n");

		int ch;

		printf("Enter your choice\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				{
					int nodeCount;
					printf("Number of Nodes you want in First Linked list:\n");
					scanf("%d",&nodeCount);

					for(int i=1;i<=nodeCount;i++){
						addNode(&head1);
					}

				}
				break;
			case 2:
				{
					int nodeCount;
					printf("Number Nodes you want in Second Linked list:\n");
					scanf("%d",&nodeCount);

					for(int i=1;i<=nodeCount;i++){
						addNode(&head2);
					}
				}
				break;	
			case 3:
				printLL();
				break;
			case 4:
				{
					int num;
					printf("Enter the number Nodes from first to Concat of 2nd LL\n");
					scanf("%d",&num);

					ConcatSLL(num);
				}
				break;	
			default:
				printf("Wrong choice,make Linked lists First\n");			
		 }
		getchar();
		printf("\nSwitch to Next Operation:");
		scanf("%c",&choice);
		
	}while(choice == 'y' || choice == 'Y');

	
}
