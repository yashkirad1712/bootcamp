/*Swap code in Doubly linked list Linked List*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}
/*
int addFirst(){
	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head->prev = newNode;
	        newNode->prev = head;
		head = newNode;	
	}
	return 0;
}  
*/

int addNode(){
	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
	return 0;
}

int countNode(){
	node *temp = head;
	int count=0;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;	
}

/*
int addatPos(int pos){
	
	int count = countNode();
	if(pos<=0 || pos>count +1){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos == count){
			addNode();
		}else if(pos == 1){
			addFirst();
		}else{
			node *newNode = createNode();

			node *temp = head;
			
			while(pos -2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next->prev = newNode;
			temp = newNode;
			newNode->prev = temp;
		}
		return 0;
	}
}  */

void printLL(){
	node *temp = head;

	while(temp->next != NULL){
		printf("| %d |->",temp->data);
		temp = temp->next;
	}
	printf("| %d |",temp->data);
}

void swapLL(){
	if(head == NULL){
		printf("Add nodesfor swapping\n");
	}else{ 
		int count = countNode();
		
		if(count == 1){
			printf("swapping conditions inappropriate\n");
		}else{
			node *temp1 = head;
			node *temp2 = head;

			while(temp2->next != NULL){
				temp2 = temp2->next;
			}	

			int cnt = count/2;
			
			
			while(cnt){
				int temp = temp1->data;
				temp1->data = temp2->data;
				temp2->data = temp;

				temp1 = temp1->next;
				temp2 = temp2->prev;
				cnt--;
				}
        		}
		}	

	printLL();	
}

void main(){
	printf("Enter the number of nodes you want for swapping\n");
	int nodeCount;
	scanf("%d",&nodeCount);
	if(nodeCount > 0){
		for(int i=1;i<=nodeCount;i++){
			addNode();
		}
	}else{
		printf("Code Exited,abnormally\n");
	}

	printf("The linked list formed is : \n");
	printLL();

	printf("\n");

	printf("After swapping linked list is : \n");
	swapLL();
	printf("\n");

}


























