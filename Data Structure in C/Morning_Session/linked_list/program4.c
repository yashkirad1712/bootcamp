/*Implementing midnode with optimised time complexity*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;


node * createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

int addNode(){

	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
	return 0;
}


int midNode(){
	node *fast = head;
	node *slow = head;
	

	/*
	 applicable only when:
	 given no. of nodes =4;
	 mid node will = 3;

	and if condition is
        mid node will =2;
	then.....
	fast =  head->next;
	must be done at declaration.	
	 */


	while(fast != NULL  &&  fast->next != NULL){

		fast = fast->next->next;
		slow = slow ->next;
	}
	return slow->data;
}

void main(){
	printf("Enter the number of nodes : ");
	int nodeCount;
	scanf("%d",&nodeCount);
		for(int i=1 ;i<=nodeCount ;i++){
			addNode();
		}
	printf("\n");
	printf("%d is the midvalue\n",midNode());	
}

















