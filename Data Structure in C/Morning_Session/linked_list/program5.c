/*Implementing Pallindrome Single Linked list */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;
node *head2 = NULL;
int count = 0;

node * createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

int addNode(){

	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
	return 0;
}


node* midNode(){
	node *fast = head;
	node *slow = head;


	while(fast != NULL  &&  fast->next != NULL){
		count++;
		fast = fast->next->next;
		slow = slow ->next;
	}
	return slow;
}

int inplaceReverse(){
	node *ptr = midNode();
	node *temp = ptr->next;
	
	node *temp2 = NULL;

	while(temp != NULL){
	node *temp1 = temp->next;
	temp->next = temp2;
	temp2 = temp;
	temp = temp1;
	}
	head2 = temp2;

}

int pallindrome(){
	node *temp1 = head;
	node *temp2 = head2;

	while(count){
		if(temp1->data != temp2->data){
			return 1;
		}
		temp1 = temp1->next;
		temp2  = temp2->next;
		count --;	
	}
}

void printLL(){
	node *temp = head;

	while(temp->next != NULL){
		printf("| %d |->",temp->data);
		temp = temp->next;
	}
	printf("| %d |\n",temp->data);

}


void main(){
	printf("Enter the number of nodes : ");
	int nodeCount;
	scanf("%d",&nodeCount);
		for(int i=1 ;i<=nodeCount ;i++){
			addNode();
		}
		
	printLL();
	inplaceReverse();

	int val = pallindrome();
	if(val == 1){
		printf("Not a Pallindrome\n");
	}else{
		printf("Is Pallindrome\n");
	}
	
	printf("The Original Linked list is:");
	int num = 0;
	num = count+1;
	inplaceReverse();
	printLL();
	printf("\n");
}

































