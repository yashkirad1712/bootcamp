/*Inplace Reverse code in Singly Circular linked list Linked List*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}


int addNode(){
	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
		newNode->next = head;
	}else{
		node *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		temp->next = head;
	}
	return 0;
}


void printLL(){
	node *temp = head;

	while(temp->next != NULL){
		printf("| %d |->",temp->data);
		temp = temp->next;
	}
	printf("| %d |",temp->data);
}

void IPRLL(){ 
		node *temp = NULL;
		while(head != NULL){
			temp = head->prev;	
			head->prev = head->next;
			head->next = temp;
			head = head->prev;

		}
		head = temp->prev;		
}

void main(){

	printf("Enter the number of nodes you want for reverse in SCLL\n");
	int nodeCount;
	scanf("%d",&nodeCount);
	if(nodeCount > 0){
		for(int i=1;i<=nodeCount;i++){
			addNode();
		}
	}else{
		printf("Code Exited,abnormally\n");
	}


	printf("After Inplace reverse linked list is : \n");
	IPRLL();
	printLL();
	printf("\n");

}


























