/*Concatination of TWO Singly Linked list*/
/*1. complete Singly linked list concatination*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

void addNode(node **head){
	node *newNode = createNode();
	
	if(*head == NULL){
		*head = newNode;
	}else{
		node *temp = *head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
	
}

void printLL(){
		if(head1 == NULL){
			printf("Linked List is empty\n");
		}else{
		node *temp = head1;
		while(temp->next != NULL){
			printf("| %d |->",temp->data);
			temp = temp->next;
		}
		printf("| %d |",temp->data);
	}
}

void conCatLL(){
	node *temp = head1;

	while(temp->next != NULL){
		temp = temp->next;
	}
	temp->next = head2;
	
}

void main(){
	int nodeCount;

	printf("Enter the nodeCount :Linked List 1\n");
	scanf("%d",&nodeCount);

	for(int i=1;i<=nodeCount;i++){
		addNode(&head1);
	}

	printf("Enter the nodeCount :Linked List 2\n");
	scanf("%d",&nodeCount);

	for(int i=1;i<=nodeCount;i++){
             addNode(&head2);
        }
	
	conCatLL();

	printf("The Concatted List is: \n");
	printLL();
}
