
/*Basic Tree Template Using Level of the Tree*/

#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int data;
    struct node* left;
    struct node* right;
}node;


struct node* insertNode(int level) {
	
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter the data in the Node\n");
	scanf("%d",&(newNode->data));
	
	level ++;

	getchar();
	printf("Do you want to Add Left child at level:%d :-> ",level);
	char choice;
	scanf("%c",&choice);
	
	printf("\n");
	    
	if(choice == 'y' || choice == 'Y'){
	   newNode->left = insertNode(level);
	}else{
	   newNode->left == NULL;
        }
	
	getchar();
	printf("Do you want to Add Right child at level:%d :-> ",level);
	scanf("%c",&choice);
	
	printf("\n");
	    
	if(choice == 'y' || choice == 'Y'){
	   newNode->right = insertNode(level);
	}else{
	   newNode->right == NULL;
        }

	return newNode;

}


void preOrder(node *root){
	if(root == NULL){
		return;
	}

	printf("| %d |",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void InOrder(node *root){
	if(root == NULL){
		return;
	}

	InOrder(root->left);
	printf("| %d |",root->data);
	InOrder(root->right);
}

void postOrder(node *root){
	if(root == NULL){
		return;
	}

	postOrder(root->left);
	postOrder(root->right);
	printf("| %d |",root->data);
}

void main() {
    
    	printf("Create binary search tree recursively:\n");
	node *root = (node*)malloc(sizeof(node));
	
	
	    printf("Enter the data in the Root node\n");
	    scanf("%d",&(root->data));

	    printf("          The tree Rooted with %d\n",root->data);


	getchar();
	printf("Do you want to Add Left child to the root Node : ");
	char choice;
	scanf("%c",&choice);
	
	printf("\n");
	    
	if(choice == 'y' || choice == 'Y'){
	   root->left = insertNode(0);
	}else{
	   root->left == NULL;
        }
    
    
    getchar();
    printf("Do you want to Add Right child to the root Node :");
    scanf("%c",&choice);
    
    printf("\n");

    if(choice == 'y' || choice == 'Y'){
	root->right = insertNode(0);
    }else{
    	root->right == NULL;
    }		
    
    
    char ch;
    do{
    	printf("|1. preOrder B-Tree | \n");
    	printf("|2. InOrder B-Tree  | \n");
    	printf("|3. postOrder B-Tree| \n");

	printf("Choose the Order to print\n");
	int num;
	scanf("%d",&num);

	switch(num){
		
		case 1 :
			preOrder(root);
			break;

		case 2 :
			InOrder(root);
			break;

		case 3 :
			postOrder(root);
			break;
		default :
			printf("Invalid Input\n");
			break;
	}
	printf("\nDo you want to continue :");
	getchar();
	scanf("%c",&ch);
    }while(ch == 'y' || ch == 'Y');
}
