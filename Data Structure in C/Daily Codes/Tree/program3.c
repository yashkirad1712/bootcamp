
/*Creating Tree from the Inorder and preOrder elements in an Array */


#include <stdio.h>
#include <stdlib.h>

typedef struct TreeNode {
    int data;
    struct TreeNode* left;
    struct TreeNode* right;
} TreeNode;

int RootIndex = 0;

TreeNode* CreateTree(int Inorder[], int PreOrder[], int start, int end) {
    if (start > end) {
        return NULL;
    }

    TreeNode* newNode = (TreeNode*)malloc(sizeof(TreeNode));
    newNode->data = PreOrder[RootIndex];
    
    int index = -1;

    for(int i= start ; i <= end ; i++){
    	if(Inorder[i] == PreOrder[RootIndex]){
		index = i;
		break;
	}
    }

    RootIndex++;

    if (start == end) {
        newNode->left = NULL;
        newNode->right = NULL;
        return newNode;
    }
	
    newNode->left = CreateTree(Inorder, PreOrder, start, index - 1);
    newNode->right = CreateTree(Inorder, PreOrder, index + 1, end);

    return newNode;
}


void PrintTree(TreeNode* root) {
    if (root == NULL) {
        return;
    }

    PrintTree(root->left);
    printf("%d ", root->data);
    PrintTree(root->right);
}

int main() {
    int Inorder[] = {2, 5, 4, 1, 6, 3, 8, 7};
    int PreOrder[] = {1, 2, 4, 5, 3, 6, 7, 8};
   
    
    int size = (sizeof(Inorder)/sizeof(Inorder[0]))-1;
    
    TreeNode* root = CreateTree(Inorder, PreOrder,0,size);
    printf("\n");
    
    PrintTree(root);
    return 0;
}

