
/*Creating Tree from the Inorder and postOrder elements in an Array */


#include <stdio.h>
#include <stdlib.h>

typedef struct TreeNode {
    int data;
    struct TreeNode* left;
    struct TreeNode* right;
} TreeNode;

int RootIndex = 7;

TreeNode* CreateTree(int Inorder[], int PostOrder[], int start, int end) {
    if (start > end) {
        return NULL;
    }

    TreeNode* newNode = (TreeNode*)malloc(sizeof(TreeNode));
    newNode->data = PostOrder[RootIndex];
    
    int index = -1;

    for(int i= start ; i <= end ; i++){
    	if(Inorder[i] == PostOrder[RootIndex]){
		index = i;
		break;
	}
    }

    RootIndex--;

    if (start == end) {
        newNode->left = NULL;
        newNode->right = NULL;
        return newNode;
    }
	
    newNode->left = CreateTree(Inorder, PostOrder, start, index - 1);
    newNode->right = CreateTree(Inorder, PostOrder, index + 1, end);

    return newNode;
}


void PrintTree(TreeNode* root) {
    if (root == NULL) {
        return;
    }

    PrintTree(root->left);
    printf("%d ", root->data);
    PrintTree(root->right);
}

int main() {
    int Inorder[] = {2, 5, 4, 1, 6, 3, 8, 7};
    int PostOrder[] = {5, 4, 2, 6, 8, 7, 3, 1};
   
    
    int size = (sizeof(Inorder)/sizeof(Inorder[0]))-1;
    
    TreeNode* root = CreateTree(Inorder, PostOrder,0,size);
    printf("\n");
    
    PrintTree(root);
    return 0;
}

