/*Tree Template using String*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
    int data;
    struct node* left;
    struct node* right;
};

struct node* newNode(int data) {
    struct node* node = (struct node*)malloc(sizeof(struct node));
    node->data = data;
    node->left = NULL;
    node->right = NULL;
    return node;
}

struct node* insert(struct node* root) {
    char input[10];
    int data;
    printf("Enter data for node : ");
    gets(input);
    if (strcmp(input, "N") == 0) {
        return NULL;
    }
    sscanf(input ,"%d", &data);
    struct node* node = newNode(data);
    printf("Do you want to Enter data for left child node : ");
    gets(input);
    if (strcmp(input, "N") != 0) {
        node->left = insert(node->left);
    }
    printf("Do you want to Enter data for right child node : ");
    gets(input);
    if (strcmp(input, "N") != 0) {
        node->right = insert(node->right);
    }
    return node;
}

void inorderTraversal(struct node* root) {
    if (root != NULL) {
        inorderTraversal(root->left);
        printf("%d ", root->data);
        inorderTraversal(root->right);
    }
}

void main() {
    struct node* root = NULL;
    printf("Create binary search tree recursively:\n");
    root = insert(root);
    printf("In-order traversal of binary search tree: ");
    inorderTraversal(root);

    printf("\n");
    
}
