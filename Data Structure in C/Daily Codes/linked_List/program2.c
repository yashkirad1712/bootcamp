/*Singly linkned list all basic functions*/

#include<stdio.h>
#include<stdlib.h>

typedef struct node {
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	
	printf("Enter the data: \n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

int addNode(){
	node *newNode = createNode();

	if(head == NULL){
		head = newNode; 
	}else{
		node *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
	return 0;

}

int addFirst(){
	node *newNode = createNode();
	
	if(head == NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head = newNode;
	}
	return 0;	
}

int addAtPos(int pos){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;
		while(pos-2){
			temp = temp->next;
			pos--;
		}
		newNode->next = temp->next;
		temp->next = newNode;
	}
	return 0;
}


int delFirst(){
	if(head == NULL){
		return -1;
	}else{
		node *temp = head;
		head = head->next;
		free(temp);
		return 0;
	}	
}

int delAtPos(int pos){
	if(head == NULL){
		return -1;
	}else{
		node *temp = head;
		while(pos - 2){
			temp = temp->next;
			pos--;
		}

		node *temp1 = temp->next;
		temp->next = temp1->next;
		free(temp1);
		return 0;
	}
}

int delLast(){
	if(head == NULL){
		return -1;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		node *temp1 = temp;
		free(temp->next);
		temp1->next = NULL;
		return 0;
	}
}

void printLL(){
	node *temp = head;
	while(temp->next != NULL){
		printf("| %d |->",temp->data);
		temp = temp->next;
	}
	printf("| %d |",temp->data);
}

void main(){
	printf("Checking Of SinglyLinked list\n");

	addFirst();
	addNode();
	addNode();

	printf("Enter the Position\n");
	int pos;
	scanf("%d",&pos);
	addAtPos(pos);
	addNode();
	printf("After Adding Elements\n");
	printLL();
	printf("\n");
	delFirst();
	delLast();
	delAtPos(pos);
	printf("After deleting Elements\n");
	printLL();
	printf("\n");

}

































