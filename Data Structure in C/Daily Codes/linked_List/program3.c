/*Creating alinked list which will add the nodes from left to right
 * ex 143 
 *   +292
 *   =336*/

#include<stdio.h>
#include<stdlib.h>


typedef struct Node{
	
	int data;
	struct Node* next;
}Node;

Node *head1 = NULL;
Node *head2 = NULL;
Node *head3 = NULL;
int num = 0,carry =0;

Node* CreateNode(){
	
	Node* newNode = malloc(sizeof(Node));

	printf("Enter the Value\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;
	return newNode;
}

void AddNode1(){
	
	Node* newNode = CreateNode();

	if(head1 == NULL){
		
		head1 = newNode;
	}else{
		
		Node *temp = head1;

		while(temp -> next != NULL){
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void AddNode2(){
	
	Node* newNode = CreateNode();

	if(head2 == NULL){
		
		head2 = newNode;
	}else{
		
		Node *temp = head2;

		while(temp -> next != NULL){
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void CreateLinkedList(){
	
	printf("Adding the Numbers Left to right\n");

	while(num){
		int sum = 0;
		sum = head1->data + head2->data + carry;
	
		Node* newNode = malloc(sizeof(Node));
		if(sum/10 != 0){
			carry = sum%10;
			newNode->data = sum/10;
			
		}else{
			newNode->data = sum%10;
			carry = 0;
		}	
		
		if(head3 == NULL){
			head3 = newNode;
		}else{
			Node* temp = head3;
			while(temp->next != NULL){
				temp = temp->next;
			}
			temp->next = newNode;
		}
		head1 = head1->next;
		head2 = head2->next;
		num--;
	}
}

void PrintNode(Node *head){
	
	Node *temp = head;
	while(temp->next != NULL){
		
		printf("| %d |->",temp->data);
		temp = temp->next;
	}

	printf("| %d |\n",temp->data);
}


void main(){

	printf("Enter the number of digits in First Number\n");
	scanf("%d",&num);

	while(num){
		AddNode1();
		num--;
	}
	
	printf("Enter the number of digits in Second Number\n");
	scanf("%d",&num);

	while(num){
		AddNode2();
		num--;
	}
	
	PrintNode(head1);
	PrintNode(head2);
	
	
	CreateLinkedList();
	PrintNode(head3);
}
