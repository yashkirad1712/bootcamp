/*Inplace reverse in singly linked list*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {
	int data;
	struct Node *next;
}node;

node *head = NULL;

node * createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

int addNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
	return 0;
}


int printLL(){
	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		node *temp = head;

		while(temp->next != NULL){
			printf("| %d |->",temp->data);
			temp = temp->next;
		}
		printf("| %d |",temp->data);
		return 0;
	}
}
int countNode(){
	node *temp = head;
	int count =0;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

int IPRSLL(){
	if(head == NULL || head->next == NULL){
		printf("Please use suitable conditions for Swapping\n");
		return -1;
	}else{
		node *temp = head;
		node *temp1 = NULL;
		node *temp2 = NULL;

		while(head != NULL){
			temp2 = head->next;
			head->next = temp1;
			temp1 = head;
			head = temp2;
		}
		head2 = temp1;
         }  
}	


void main(){
	printf("Enter the number of nodes\n");
	int nodecount;
	scanf("%d",&nodecount);

	for(int i=1;i<=nodecount;i++){
		addNode();
	}
	printf("The linked list formed is :\n");
	printLL();
	printf("\n");
	printf("The linked list formed after inplace reverse is :\n");
	IPRSLL();
	printLL();
	printf("\n");	
}


















