/*implementing enqueue ,dequeue ,front , printqueue operations in array*/

#include<stdio.h>

int front = -1;
int rear = -1;
int size = 0;
int flag = 0;

int enqueue(int arr[]){
	if(rear == size-1){
		return -1;
	}else{
		if(front == -1){
			front++;
		}
		rear++;
		printf("Enter the data\n");
		scanf("%d",&arr[rear]);
		return 0;
	}	
}

int dequeue(int arr[]){
	if(front == -1){
		flag = 1;
		return -1;
	}else{
		int val = arr[front];
		flag = 0;
		if(front == rear){
			front = -1;
			rear = -1;
		}else{
			front ++;
		}
		return val;
	}
}

int Front(int arr[]){
	if(front == -1){
		return -1;
	}else{
		return arr[front];
	}
}



int printQueue(int arr[]){
	if(front == -1){
		return -1;
	}else{
		for(int i=front; i<=rear; i++ ){
			printf("| %d |",arr[i]);
		}
		return 0;
	}
}

void main(){
	printf("Enter the size of the array: ");
	scanf("%d",&size);
	printf("\n");

	int arr[size];

	char choice;

	do{
		printf("| 1.Enqueue    |\n");
		printf("| 2.Dequeue    |\n");
		printf("| 3.Front      |\n");
		printf("| 4.printQueue |\n");

		int ch;
		printf("Enter your choice\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				{
					int ret = enqueue(arr);
					if(ret == -1){
						printf("Queue overflow\n");
					}else{
						printf("/------Successfully enqued----/\n");
					}
				}
				break;

			case 2:
				{
					int ret = dequeue(arr);
					if(flag == 1){
						printf("Stack underflow\n");
					}else{
						printf("%d is dequeued\n",ret);
					}
				}
				break;

			case 3:
				{
					int ret = Front(arr);
					if(ret == -1){
						printf("Queue is empty\n");
					}else{
						printf("Front = %d\n",ret);
					}
				}
				break;

			case 4:
				{
					int ret = printQueue(arr);
					if(ret == 1){
						printf("Queue is empty\n");
					}else{
						printf("/---------Queue is printted successfully-------/\n");
					}
				}
				break;
			
			default:
				printf("Invalid input\n");
		}
		getchar();
		printf("\nDo you want to continue :");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}








