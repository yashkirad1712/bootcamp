/*Optimised Queue using Linked list*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *front = NULL;
node *rear = NULL;
int flag = 0;

node * createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}


int Enqueue(){

	node *newNode = createNode();

	if(newNode == NULL){
		printf("Memory Full...\n");
		exit(0);
	}else{	
		
		if(front == NULL){
			front = newNode;
			rear = newNode;		
		}else{
			rear->next = newNode;
			rear = newNode;
		}
		return 0;
	}	
}

int Dequeue(){
	if(front == NULL){
		flag = 1;
		return -1;
	}else{
		int val = front->data;
		if(front == rear){
			free(front);
			front = NULL;
			rear = NULL;
		}else{
			node *temp = front;
			front = front->next;
			free(temp);
		}
		return val;
	}	
}

int Front(){
	if(front == NULL){
		return -1;
	}else{
		return front->data;
	}
}

int printLL(){
	if(front == NULL){
		return -1;
	}else{
		node *temp = front;

		while(temp->next != NULL){
			printf("| %d |->",temp->data);
			temp = temp->next;
		}
		printf("| %d |",temp->data);
		return 0;
	}
}

void main(){
	printf("Welcome to the Queue _||_ \n");

	char choice;

	do{
		printf("| 1.Enqueue     |\n");
		printf("| 2.Dequeue     |\n");
		printf("| 3.Front       |\n");
		printf("| 4.PrintLL     |\n");

		int ch;
		printf("Enter your choice : ");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				{
					int ret = Enqueue();
					if(ret == -1){
						printf("Queue Overflow\n");
					}else{
						printf("----/Enqueued successfully/----\n");
					}
				}
				break;

			case 2:
				{
					int ret = Dequeue();
					if(flag == 1){
						printf("Queue Underflow\n");
					}else{
						printf("%d is dequeued\n",ret);
					}
				}
				break;

			case 3:
				{
					int ret = Front();
					if(ret == -1){
						printf("Queue is empty\n");
					}else{
						printf("Front = %d",ret);
					}	
				}
				break;

			case 4:
				{
					int ret = printLL();
					if(ret == -1){
						printf("Queue is empty\n");
					}
				}
				break;

			default :
				printf("Invalid Input\n");
				break;

		}
		getchar();
		printf("\nDo you want to continue :");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}




















