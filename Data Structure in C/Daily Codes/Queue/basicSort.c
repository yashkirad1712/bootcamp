/*Priority wise Sorting using Linked list*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	int priority;
	struct Node *next;
}node;

node *head = NULL;
int size = 0;
int flag = 1;
int flag1 = 0;


node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the Data\n");
	scanf("%d",&newNode->data);

	do{
		printf("Enter the PRIORITY\n");
		scanf("%d",&newNode->priority);

		if(newNode->priority < 0 || newNode->priority > size){
			printf("Invalid Input\n");
		}
	}while(newNode->priority < 0 || newNode->priority > size);

	newNode->next = NULL;
}


int enqueue(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		if(head->priority > newNode->priority){
			newNode->next = head;
			head = newNode;
		}else{
			node *temp = head;

			while(temp->next != NULL){
				if(temp->next->priority > newNode->priority){
					newNode->next = temp->next;
					temp->next = newNode;
					break;

				}
				temp = temp->next;
			}
		}
		
	}
	return 0;
}

int dequeue(){
	if(head == NULL){
		flag1 = 0;
		return -1;
	}else{
		flag1 = 1;
		node *temp = head;
		int val = temp->data;
		head = head->next;
		free(temp);
		return val;
	}
}

int printqueue(){
	if(head == NULL){
		return -1;
	}else{
		node *temp = head;
	
		while(temp != NULL){
			printf("%d- %d |",temp->data,temp->priority);
			temp = temp->next;
		}
		return 0;
   }
}

void main(){
	printf("Enter the size of priority\n");
	scanf("%d",&size);

	char choice ;

	do{
		printf("|1. Enqueue     |\n");
		printf("|2. Dequeue     |\n");
		printf("|3. PrintQueue  |\n");

		int ch;
		printf("Enter your choice\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				{
					int ret = enqueue();
					if(ret == 0){
						printf("Enqueued successfully\n");
					}
				}
				break;
			case 2:
				{	
					if(flag1 == 0){
						printf("Queue is empty\n");	      
					}else{
						int val = dequeue();
						printf("%d is dequeued\n",val);
					}	
				}
				break;
			case 3:
				{
					int ret = printqueue();
					if(ret == -1){
						printf("Queue is Empty\n");
					}
				}
				break;
			default:
				printf("Invalid Input\n");
				break;	
		}
		getchar();
		printf("\nDo you want to continue : ");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}


























