/*Implementing Circular Queue using Array*/

#include<stdio.h>

int front = -1;
int rear = -1;
int size = 0;
int flag = 0;


int enqueue(int arr[]){
	if(front == -1){
		front++;
		rear++;

		printf("Enter the data\n");
		scanf("%d",&arr[rear]);
		return 0;
	}else{
		if((rear == size-1 && front == 0) || (rear == front-1)){
			return -1;
		}else{
			if(rear == size-1){
				rear = 0;
				printf("Enter the data\n");
				scanf("%d",&arr[rear]);
			}else{
				rear++;
				printf("Enter the data\n");
				scanf("%d",&arr[rear]);
			}
			return 0;
		}
	}
}

int dequeue(int arr[]){
	if(front == -1){
		flag = 0;
		return -1;

	}else{
		flag = 1;
		int val = arr[front];
		if(front == rear){
			front = -1;
			rear = -1;
		}else{
			if(front == size-1){
				front = -1;
			}
			front++;
		}
		return val;
	}
}

int Front(int arr[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return arr[front];
	}
}

int printQueue(int arr[]){
	 if(front == -1){
		return -1;
	 }else{
	         if(rear == size -1 && front == 0){
			for(int i=front ;i<=rear;i++){
				printf("| %d |",arr[i]);
			}
		}else{
			int num = front;
			while(num != rear){
                           	if(num == size-1){
					printf("| %d |",arr[num]);
					num = 0;
				}

				printf("| %d |", arr[num]);
				num++;
			}
			printf( "| %d |",arr[num]);	
		}
		return 0;
	}		
}


void main(){
	printf("Enter the size of the array\n");
	scanf("%d",&size);

	int arr[size];

	char choice;

	do{
		printf("| 1. enqueue |\n");
		printf("| 2. dequeue |\n");
		printf("| 3. Front   |\n");
		printf("| 4. PrintQ  |\n");

		printf("Enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				{
					int ret = enqueue(arr);
					if(ret == -1)
						printf("Queue is Full\n");
					else
						printf("---/Enqueued successfully/---\n");
				}
				break;
			case 2:
				{
					int ret = dequeue(arr);
					if(flag == 1){
						printf("%d is dequeued\n",ret);
					}else{
						printf("Queue is Empty\n");
					}
				}
				break;

			case 3:
				{
					int ret = Front(arr);
					if(flag == 1){
						printf("%d is at Front\n",ret);
					}else{
						printf("Queue is Empty\n");
					}
				}
				break;

			case 4:
				{
					int ret = printQueue(arr);
					if(ret == -1)
						printf("Queue is Empty\n");
					
				}
				break;	

			default:
				printf("Invalid Input\n");	
		
		}
		getchar();
		printf("\nDo you want to continue: ");
		scanf("%c",&choice);
	}while(choice =='y' || choice =='Y');
}











