/*Circular queue using Singly Circular Linked list*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *front = NULL;
node *rear = NULL;
int flag = 0;
int count = 0;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

int enqueue(int size){
	if(count == size){
		return -1;
	}else{
		node *newNode = createNode();	
		if(front == NULL){
			front = newNode;
			rear = newNode;
			rear->next = rear;
		}else{
	  		rear->next = newNode;
			newNode->next = front;
			rear = newNode;
		}
		count++;
		return 0;
	}	
}

int dequeue(){
	if(front == NULL){
		flag = 0;
		return -1;
	}else{
		int val = front->data;
		flag = 1;
		if(rear == front){
			free(rear);
			free(front);
			rear = NULL;
			front =NULL;
		}else{
			front = front->next;
			free(rear->next);
			rear->next = front;
		}
		count --;
		return val;	
	}
}

int frontt(){
	if(front == NULL){
		return -1;
	}else{
		int val = front->data;
		return val;
	}
}

int printQ(){
	if(front == NULL){
		return -1;
	}else{
		node *temp = front;
		while(temp != rear){
			printf(" | %d |",temp->data);
			temp = temp->next;
		}
		printf("| %d |",temp->data);
		return 0;
	}
}

void main(){
	printf("Enter the size of the Queue\n");
	int size;
	scanf("%d",&size);

	char choice;

	do{
		printf("|1. Enqueue      |\n");
		printf("|2. Dequeue      |\n");
		printf("|3. Front        |\n");
		printf("|4. PrintQ       |\n");

		printf("Enter your Choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				{
					int ret = enqueue(size);
					if(ret == -1)
						printf("Queue is FULL\n");
					else
						printf("--/Enqueued successfully/--\n");
				}
				break;
			case 2:
				{
					int ret = dequeue();
					if(flag == 0)
						printf("Queue is Empty\n");
					else
						printf("%d is dequeued\n",ret);
				}
				break;

			case 3:
				{
					int ret = frontt();
					if(ret == -1)
						printf("Queue is empty\n");
					else
						printf("%d is at Front\n",ret);
				}
				break;

			case 4:
				{
					int ret = printQ();
					if(ret == -1)
						printf("Queue is empty\n");
	
				}
				break;

			default :
				printf("Invalid Input\n");
				break;
		
			
		}

		getchar();
		printf("\nDo you want to Continue: ");
		scanf("%c",&choice);

	}while(choice == 'y'  || choice == 'Y');

}































