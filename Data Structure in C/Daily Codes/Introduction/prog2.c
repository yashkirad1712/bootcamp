/*creating two structures with a head pointer*/

#include<stdio.h>
#include<string.h>

typedef struct Batsman{
    int jerNo;
    char name[20];
    float avg;
    struct Batsman *next;
}bat;

typedef struct Company{
    int empcount;
    char name[20];
    float rev;
    struct Company *next;
}com;

void main(){
    bat obj1,obj2,obj3;

    bat *head1=&obj1;

    head1->jerNo=18;
    strcpy(head1->name,"Virat K");
    head1->avg=250.00;
    head1->next=&obj2;

    head1->next->jerNo=45;
    strcpy(head1->next->name,"Rohit S");
    head1->next->avg=180.00;
    head1->next->next=&obj3;

    head1->next->next->jerNo=7;
    strcpy(head1->next->next->name,"MSD");
    head1->next->next->avg=210;
    head1->next->next->next=NULL;


    printf("%d\n",head1->jerNo);
    printf("%s\n",head1->name);
    printf("%f\n",head1->avg);
    printf("\n");

    printf("%d\n",head1->next->jerNo);
    printf("%s\n",head1->next->name);
    printf("%f\n",head1->next->avg);
    printf("\n");

    printf("%d\n",head1->next->next->jerNo);
    printf("%s\n",head1->next->next->name);
    printf("%f\n",head1->next->next->avg);
    printf("\n");

    com emp1,emp2,emp3;
    com* head2=&emp1;


    head2->empcount=1;
    strcpy(head2->name,"Viren");
    head2->rev=50.00;
    head2->next=&emp2;

    head2->next->empcount=2;
    strcpy(head2->next->name,"Lee");
    head2->next->rev=80.00;
    head2->next->next=&emp3;

    head2->next->next->empcount=3;
    strcpy(head2->next->next->name,"Riya");
    head2->next->next->rev=90.00;
    head2->next=NULL;


    printf("%d\n",head2->empcount);
    printf("%s\n",head2->name);
    printf("%f\n",head2->rev);
    printf("\n");

    printf("%d\n",head2->next->empcount);
    printf("%s\n",head2->next->name);
    printf("%f\n",head2->next->rev);
    printf("\n");

    printf("%d\n",head2->next->next->empcount);
    printf("%s\n",head2->next->next->name);
    printf("%f\n",head2->next->next->rev);
    printf("\n");
}
