#include <stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie{
    char mName[20];
    float imdb;
    struct Movie* next;
}mov;

mov* head=NULL;

void addNode(){
    mov* newNode=(mov*)malloc(sizeof(mov));
    printf("Enter Movie Name\n");
    fgets(newNode->mName,15,stdin);

    printf("Enter imdb Rating\n");
    scanf("%f ",&newNode->imdb);

    getchar();

    if(head == NULL){
        head=newNode;
    }else{
        mov* temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void printLL(){
    mov*ptr=head;

    while(ptr!=NULL){
        printf("| %s ",ptr->mName);
        printf("%f |",ptr->imdb);

        ptr=ptr->next;
    }

}

void main(){
    addNode();
    addNode();
    addNode();
    printLL();

}
