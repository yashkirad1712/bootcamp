/*Creating the functions in linked list*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct movie{
    int persons;
    char movName[20];
    float duration;
    struct movie* next;
}mov;

void access(mov* ptr){
    printf("%d  |",ptr->persons);
    printf("%s  |",ptr->movName);
    printf("%f",ptr->duration);
}

void take(mov* ptr){
    scanf("%d",ptr->persons);
    fgets(ptr->movName,15,stdin);
    scanf("%f",ptr->duration);

}

void main(){
    mov* mov1=(mov*)malloc(sizeof(mov));
    mov* mov2=(mov*)malloc(sizeof(mov));
    mov* mov3=(mov*)malloc(sizeof(mov));

    take(mov1);
    access(mov1);

    take(mov2);
    access(mov2);

    take(mov3);
    access(mov3);
}
