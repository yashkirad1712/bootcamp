#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct employee{
    int empId;
    char empName[20];
    float sal;
    struct employee* next;
}emp;

void main(){
    emp* emp1=(emp*)malloc(sizeof(emp));
    emp* emp2=(emp*)malloc(sizeof(emp));
    emp* emp3=(emp*)malloc(sizeof(emp));

    emp1->empId=1;
    strcpy(emp1->empName,"Yash");
    emp1->sal=90.00;
    emp1->next=&emp2;

    emp2->empId=2;
    strcpy(emp2->empName,"Rahul");
    emp2->sal=80.00;
    emp2->next=emp3;

    emp2->next->empId=3;
    strcpy(emp2->next->empName,"Nin");
    emp2->next->sal=70.00;
    emp2->next->next=NULL;

    printf("%d  |",emp1->empId);
    printf("%s  |",emp1->empName);
    printf("%f",emp1->sal);
    printf("\n");
    printf("%d  |",emp2 ->empId);
    printf("%s  |",emp2->empName);
    printf("%f",emp2->sal);
    printf("\n");
    printf("%d  |",emp3->empId);
    printf("%s  |",emp3->empName);
    printf("%f",emp3->sal);




}
