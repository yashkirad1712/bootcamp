/*Introduction to linked list*/
#include<stdio.h>
#include<string.h>

typedef struct employee{
    int empId;
    char empName[20];
    float sal;
    struct employee* next;
} emp;

void main(){
    emp obj1,obj2,obj3;


    obj1.empId=1;
    strcpy(obj1.empName,"Yash");
    obj1.sal=80.00;
    obj1.next=&obj2;

    obj1.empId=1;
    strcpy(obj1.empName,"Yash");
    obj1.sal=80.00;
    obj1.next=&obj2;

    obj1.next->empId=2;
    strcpy(obj1.next->empName,"Nin");
    obj1.next->sal=70.00;
    obj1.next->next=&obj3;

    obj3.empId=3;
    strcpy(obj3.empName,"Sai");
    obj3.sal=70.00;
    obj3.next=NULL;

    emp*head=&obj1;
    printf("%d\n",head->empId);
    printf("%s\n",head->empName);
    printf("%f\n",head->sal);

    printf("%d\n",obj2.empId);
    printf("%s\n",obj2.empName);
    printf("%f\n",obj2.sal);

    printf("%d\n",obj2.next->empId);
    printf("%s\n",obj2.next->empName);
    printf("%f\n",obj2.next->sal);



}
