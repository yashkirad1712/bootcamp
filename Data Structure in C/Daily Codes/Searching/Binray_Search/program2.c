/*Ceiling Value from Binary search*/

/*Ceiling : the closest greater value or the equal value to the key value*/

#include<stdio.h>

int CeilingVal(int arr[],int size,int key){
	int start = 0;
	int end = size - 1;
	
	int store = -1;
	
	while(start <= end){
		int mid = (start + end)/2;

		if(arr[end] < key){
			return end;
		}

		if(arr[start] > key){
			return start;
		}

		if(arr[mid] == key){
			return mid;
		}

		if(arr[mid] > key){
		        store = mid;
			end = mid - 1;
		}

		if(arr[mid] < key){
			start = mid + 1;
		}
	}
	return store;
}

void main(){
	int arr[] = {1,2,3,5,9,7,8};

	printf("Enter the value of key\n");
	int key;
	scanf("%d",&key);

	int ret = CeilingVal(arr,7,key);

	printf("The Ceiling value is %d\n",ret);
}
