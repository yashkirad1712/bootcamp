/*Floor Value:the closest smaller value or the equal value to the given key*/

#include<stdio.h>

int FloorVal(int arr[],int size,int key){
	int start = 0;
	int end = size-1;

	int store = -1;
	
	while(start <= end){
		int mid = (start + end)/2;

		if(arr[mid] == key){
			return mid;
		}

		if(arr[mid] > key){
			end = mid - 1;
		}

		if(arr[mid] < mid){
			store = mid;
			start = mid + 1;
		}	
	}
	return store;
}

void main(){
	int arr[]  = {1,2,5,6,7,8,9};

	int key;
	printf("Enter the value of key\n");
	scanf("%d",&key);
	
	int ret = FloorVal(arr,7,key);

	if(ret == -1)
		printf("Element is not Present\n");
	else
		printf("The Floor value of the given key is at %d\n index",ret);

}
