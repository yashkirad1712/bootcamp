/*Rotate Array : Here the given array is sorted but it is rotated N no. of times and we have to find the peak value from the array and then search for the key element as per the peak value*/

#include<stdio.h>
int peakVal(int arr[],int size){
	int peak = 0;

	for(int i =1 ;i<size;i++){
		if(arr[i] > arr[peak])
		    	peak = i;
	}
	return peak;
}
int RotateArray(int arr[],int size,int key){
	int peak = peakVal(arr,size);
	
	if(arr[peak] == key){
		return peak;
	}

	printf("The peak value is %d\n",arr[peak]);
	int start = 0;
	int end = size - 1;
	
	if(key > arr[end]){
		while(start <= peak){
			int mid = (start + peak)/2;

			if(arr[mid]== key){
				return mid;
			}

			if(arr[mid] > key){
				peak = mid - 1;
			}

			if(arr[mid] < key){
				start = mid +1;
			}
		}
		return -1;
	}                        

	if(key < arr[end]){
		while(peak <= end){
			int mid = (peak + end)/2;

			if(arr[mid]== key){
				return mid;
			}

			if(arr[mid] > key){
				end = mid - 1;
			}

			if(arr[mid] < key){
				peak = mid +1;
			}
		}
		return -1;
	}                        
	
}

void main(){
	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);

	int arr[size];
	printf("Enter the elements of the array\n");
	for(int i=0 ; i<7 ;i++){
		scanf("%d",&arr[i]);
	}
	
	printf("The Array is:\n");
	for(int i=0 ; i<7 ;i++){
		printf("|%d|",arr[i]);
	}

	printf("\nEnter the key\n");
	int key;
	scanf("%d",&key);
	
	int ret = RotateArray(arr,7,key);
	
	if(ret == -1)
		printf("The Given key is not present\n");
	else
		printf("The given key is at %d index\n",ret);
}













