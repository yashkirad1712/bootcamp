/*Binary Search:here we take the mid values of the array and check whether that value satisfies the condition*/

#include<stdio.h>

int BinarySearch(int arr[],int size,int key){ 
	int mid = 0;
	int end = size-1;
	int start = 0;

	while(start <= end){
		mid = (start + end)/2;

		if(arr[mid] == key){
			return mid;
		}

		if(arr[mid] > key){
			end = mid - 1;
		}

		if(arr[mid] < key){
			start = mid + 1;
		}
	}

	return -1;
}

void main(){
	printf("The array is\n");
	int arr[] = {1,2,3,4,5};
	
	int key;
	printf("Enter the value of key\n");
	scanf("%d",&key);

	int ret = BinarySearch(arr,5,key);
	if(ret == -1)
		printf("Key is not present\n");
	else
		printf("The key is found at %d index",ret);

}
