/*First occurence from binary search*/

#include<stdio.h>

int FirstOccr(int arr[],int size,int key){
	int start =0;
	int end = size - 1;
	if(arr[0] > key || arr[end] < key){
		return -1;
	}
	while(start <= end){
		int mid = (start + end)/2;

		if(arr[mid] == key){
			return mid;
		}
		if(arr[mid] > key){
			end = mid - 1;
		}
		if(arr[mid] < key){
			start = mid +1;
		}

	}
	return -1;
}

void main(){
	int arr[] = {1,5,6,7,3,9,10};

	int key ;
	printf("Enter the key\n");
	scanf("%d",&key);

	int ret = FirstOccr(arr,7,key);

	if(ret == -1)
		printf("The key is Not present\n");
	else
		printf("The key is at %d index\n",ret);

}
