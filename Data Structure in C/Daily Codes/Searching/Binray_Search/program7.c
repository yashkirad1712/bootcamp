/*Second Last Element to searched similar to the key and return th eindex of th eelement*/

#include<stdio.h>

int LastOccr(int arr[],int size,int key){
	int start =0;
	int end = size - 1;
	int store = -1;
	int store1 = -1;
	if(arr[0] > key || arr[end] < key){
		return -1;
	}

	while(start <= end){
		int mid = (start + end)/2;

		if(arr[mid] == key){
			store1 = store;
			store = mid;
			start = mid +1;
		}
		if(arr[mid] > key){
			end = mid - 1;
		}
		if(arr[mid] < key){
			start = mid +1;
		}

	}
	return store1;
}

void main(){
	int arr[] = {1,2,6,7,7,7,10};

	int key ;
	printf("Enter the key\n");
	scanf("%d",&key);

	int ret = LastOccr(arr,7,key);

	if(ret == -1)
		printf("The key is Not present\n");
	else
		printf("The key is at %d index\n",ret);

}
