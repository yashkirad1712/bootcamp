/*Push Pop Operations Using Linked lIst*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	
	int data;
	struct Node *next;
}Node;

Node *head = NULL;
Node *top = NULL;

Node* createNode(int data){
	
	Node *newNode = malloc(sizeof(Node));

	printf("Enter the data\n");
	newNode->data = data;

	newNode->next = NULL;

	return newNode;
}

int push(int data){
	
	Node *newNode = createNode(data);
	if(head == NULL){
		head = newNode;
		top = newNode;
	}else{
		top->next = newNode;
		top = newNode;
	}

	return 0;
}

int pop(){
	Node *temp = head;

	while(temp->next != top){
		temp = temp->next;
	}
	int ret = top->data;
	Node* new = top->next;
	free(top);
	top = temp;
	top->next = new;

	return ret;
}

void print(){
	
	Node *temp = head;
	while(temp != NULL){
		printf("%d\n",temp->data);
		temp = temp->next;
	}
}

void main(){
	
	printf("Lets start the Stack Operations\n");
	push(10);
	push(20);
	push(30);
	int val = pop();
	push(40);
	push(50);
	push(60);
	int ret = pop();

	printf("%d | %d \n",val,ret);
	printf("This are Popped\n");
	printf("The stack has elements\n");
	print();
}


