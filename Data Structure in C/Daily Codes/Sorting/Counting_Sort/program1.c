/*Counting Sort:
  1.has Time comp,exity equal to  O(n)
  2.it has 6 steps whic increases the space complexity
  3.it is only used when the array has the elements in a given range */

#include<stdio.h>

void CountingArray(int arr[],int size){

	/*Finding the maximum eelement for creating the next array of that size+1*/
	int max = arr[0];
	for(int i=1;i<size;i++){

		if(max < arr[i])
			max = arr[i];
	}
	
	/*Assigning all the elements of the count array with 0*/
	int CountArr[max+1];
	for(int i=0;i<=max;i++){
		CountArr[i] = 0;
	}
	
	/*Counting the number of elements present in the main array and storing them in the count array*/
	for(int i=0;i<size;i++){
		CountArr[arr[i]]++;
	}

	/*updating the count array into the cummulative sum array of the count array*/
	for(int i=1;i<=max;i++){
		CountArr[i] = CountArr[i] + CountArr[i-1];
	}

	/*Craeting the other array as output for storing the data whic we have to find after the sort*/
	int output[size];
	for(int i=size-1;i>=0;i--){
		output[CountArr[arr[i]] - 1] = arr[i];
		CountArr[arr[i]]--;	
	}

	/*Now copying the elemnts of the output array into the Original array*/
	for(int i=0;i<size;i++){
		arr[i] = output[i];
	}

}


void main(){
	
	int arr[]={3,7,2,1,8,2,5,2,7};
	int size = sizeof(arr)/sizeof(arr[0]);

	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");

	CountingArray(arr,size);	
	
	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");

}
