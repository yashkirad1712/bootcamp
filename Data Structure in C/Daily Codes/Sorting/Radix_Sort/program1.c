/*RAdix Sort: here the logic is same as the counting array sort but the only difference is that here it works on the digit of the number of the elements of the array*/

#include<stdio.h>

void CountArray(int arr[],int size,int pos){

		int CountArr[10] = {0};

		for(int i=0;i<size;i++){
			CountArr[(arr[i]/pos)%10]++;
		}

		for(int i=1;i<10;i++){
			CountArr[i] += CountArr[i-1];
		}
		int output[size];
		for(int i=0;i<size;i++){
			output[CountArr[(arr[i]/pos)%10]-1] = arr[i];
			CountArr[(arr[i]/pos)%10]--;
		}

		for(int i=0;i<size;i++){
			arr[i] = output[i];
		}
}


void RadixSort(int arr[],int size){
	
	int max=arr[0];
	for(int i =1;i<size;i++){
		if(max < arr[i])
			max = arr[i];
	}

	for(int pos=1;max/pos > 0;pos=pos*10){
		CountArray(arr,size,pos);
	}
}


void main(){
	int arr[10] = {720,435,1235,920,5,8,22,115};
	int size = sizeof(arr)/sizeof(arr[0]);

	for(int i=0;i<10;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");

	RadixSort(arr,size);
	
	for(int i=0;i<10;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");

}
