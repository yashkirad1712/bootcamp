/*Selection Sort*/
 /*In this we deal with the first element of the array ,and brings the min element to the first position of the array*/

#include<stdio.h>

void SelectionSort(int arr[],int size){
	for(int i=0 ;i<size-1;i++){
		int minIndex = i;
		for(int j=i+1 ;j<size;j++){
			if(arr[minIndex]>arr[j]){
				minIndex = j;
			}
		}

		int temp = arr[i];
		arr[i] = arr[minIndex];
		arr[minIndex]= temp;
	}
}

void main(){
	int arr[] = {4,3,8,-4,2,9,7};

	int size = sizeof(arr)/sizeof(arr[0]);

	printf("The Array is \n");
	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");
	
	SelectionSort(arr,size);

	printf("The Sorted Array is \n");
	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");
}	
