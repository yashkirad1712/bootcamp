/*Haoure's Algorithm*/

#include<stdio.h>

void swap(int *ptr,int*ptr1){
	int temp = *ptr;
	*ptr = *ptr1;
	*ptr1 = temp;	
}

int partition(int arr[],int start,int end){
	int i = start -1;
       	int j = end +1;
	int pivot = arr[start];
	while(1){
		do{
			i++;
		}while(arr[i] < pivot);

		do{
			j--;
		}while(arr[j] > pivot);

		if(i >= j){
			return j;
		}
		swap(&arr[i],&arr[j]);
	}	
}

void QuickSort(int arr[],int start,int end){
	if(start < end){
		int pivot = partition(arr,start,end);
		printf("%d\n",pivot);
		QuickSort(arr,start,pivot);
		QuickSort(arr,pivot+1,end);
	}	
}

void main(){
	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);

	int arr[size];
	printf("Enter the elements of the array\n");
	for(int i=0 ;i<size;i++){
		scanf("%d",&arr[i]);
	}
	
	printf("The array is\n");
	
	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}

	QuickSort(arr,0,size-1);
	
	printf("The Sorted array is\n");
	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");

}
