/*Quick Sort :Pivot element is choosen and then it is fixed to its position*/
/*left side of the pivot element is smaller the the pivot & right side of the pivot is greater then the pivot*/
/*After fixing the pivot the next element is choosen a pivot */

#include<stdio.h>
void swap(int *val1,int *val2){
	int temp = *val1;
	*val1 = *val2;
	*val2 = temp;
}

int partition(int arr[],int start,int end){
	int itr = start - 1;
	int pivot = arr[end];

	for(int i= start ;i<end;i++){
		if(arr[i] <= pivot){
			itr++;
			swap(&arr[itr],&arr[i]);
		}
	}

	itr++;
	swap(&arr[itr],&arr[end]);
	return (itr);
}

void QuickSort(int arr[],int start, int end){
	if(start < end){
		int pivot = partition(arr,start,end);
		QuickSort(arr,start,(pivot-1));
		QuickSort(arr,(pivot+1),end);
	}	
}

void main(){
	printf("The given array is\n");
	int arr[]={8,7,2,1,0,6,9};
	int size = sizeof(arr)/sizeof(arr[0]);
	for(int i=0 ;i<size ;i++){
		printf("| %d |",arr[i]);
	}
	int end = size-1;
	QuickSort(arr,0,end);
	printf("\nThe sorted array is\n");
	for(int i=0 ;i<size ;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");	
}
