/*Quick Sort: Neive Method*/

#include<stdio.h>

int partition(int arr[],int start,int end){
	int size = end - start + 1;
	int temp[size];
	int index = -1;
	int pivot = arr[end];

	for(int i=start ;i<end;i++){
		if(arr[i] < pivot){
			index++;
			temp[index] = arr[i];
		}
	}
	index++;
	temp[index] = pivot;
	int pos = index + start;

	for(int i= start;i<end; i++){
		if(arr[i] > pivot){
			index++;
			temp[index] = arr[i];
		}
	}

	for(int i= start ;i<=end;i++){
		arr[i] = temp[i-start];
	}

	return pos;
}

void QuickSort(int arr[],int start,int end){
	if(start < end){
		int pivot = partition(arr,start,end);
		QuickSort(arr,start,pivot-1);
		QuickSort(arr,pivot+1,end);
	}
}

void main(){
	printf("Enter the size of the array \n");
	int size;
	scanf("%d",&size);
	int arr[size];

	printf("Enter the elements of the array\n");
	for(int i= 0 ;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("The array is \n");
	for(int i= 0 ;i<size;i++){
		printf("| %d |",arr[i]);
	}

	QuickSort(arr,0,size-1);

	printf("\nThe sorted array is\n");
	for(int i= 0 ;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");

}

