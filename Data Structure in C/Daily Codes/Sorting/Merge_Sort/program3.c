/*Merge sort */

/* Find the "k smallest" element in the given array after sorting it using merge sort*/

#include<stdio.h>
#include<math.h>

void Sort(int arr[],int start,int mid,int end){
	int ele1 = (mid - start + 1);
	int ele2 = (end - mid);
	int arr1[ele1],arr2[ele2];
	
	for(int i=0 ;i < ele1;i++){
		arr1[i] = arr[start + i];
	}

	for(int j=0 ;j < ele2;j++){
		arr2[j] =arr[mid+1+j]; 
	}

	int itr1 = 0,itr2 = 0,itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		if(arr1[itr1] < arr2[itr2]){
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			arr[itr3] = arr2[itr2];
			itr2++;
		}
		itr3++;

	}

	while(itr1 < ele1){
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

void MergeSort(int arr[],int start,int end){
	if(start < end){
		int mid = (start+end)/2;
		MergeSort(arr,start,mid);
		MergeSort(arr,(mid+1),end);
		Sort(arr,start,mid,end);
	}	
}

void main(){
	printf("Enter the size array\n");
	int size;
	scanf("%d",&size);
	int arr[size];
	printf("Enter the elements of the array\n");
	for(int i=0 ;i<size ;i++){
		scanf("%d",&arr[i]);
	}
	int start=0;
	int end = size-1;
	MergeSort(arr,start,end);

	printf("The sorted array is\n");

	for(int i=0 ;i<size ;i++){
		printf("%d ",arr[i]);
	}

	printf("\nEnter the smallest kth elemnt to be found \n");
	int k;
	scanf("%d",&k);
	printf("The value of smallest %d position is %d\n",k,arr[k-1]);

}	
