/*Finding the middle element from an  unsorted array without sortng it*/

#include<stdio.h>

int sort(int arr[],int start,int end){
	int mid = (start+ end)/2;
	
	for(int i = 0; i<=mid+1 ;i++){
		for(int j=0 ;j<end;j++){
			if(arr[j] > arr[j+1]){
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1]= temp;
			}
		}
	}
	return mid;
}

void main(){
	printf("Enter the size of the array\n");
	int size ;
	scanf("%d",&size);

	int arr[size];

	printf("enter the elements of the array\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("The given Array is\n");
	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}
	int ret = sort(arr,0,(size-1));
	printf("\nThe Middle index is: %d\n",ret);
	printf("The Arrays after finding middle index is\n");
	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\nThe Middle element is: %d\n",arr[ret]);
}
