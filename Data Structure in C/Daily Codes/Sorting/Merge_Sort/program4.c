/*Merge Sort*/

/*Find the "K largest" element in the given array after sorting it*/

#include<stdio.h>

void sort(int arr[],int start, int end,int mid){
	int ele1 =mid - start +1;
	int ele2 =end -mid;
	
	int arr1[ele1],arr2[ele2];

	for(int i=0 ;i<ele1;i++){
		arr1[i] = arr[start+i];
	}

	for(int j=0 ;j<ele2;j++){
		arr2[j] = arr[mid+j];
	}

	int itr1=0,itr2=0,itr3=start;

	while(itr1<ele1 && itr2<ele2){
		if(arr1[itr1] < arr2[itr2]){
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			arr[itr3] = arr2[itr2];
			itr2++;
		}
		itr3++;
	}

	while(itr1 < ele1){
		arr[itr3] = arr[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		arr[itr3] = arr[itr2];
		itr3++;
		itr2++;
	}
}

void MergeSort(int arr[],int start,int end){
	if(start < end){
		int mid = (start + end)/2;
		MergeSort(arr,start,mid);
		MergeSort(arr,mid,end);
		sort(arr,start,end,mid);
	}
}

void main(){
	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);

	int arr[size];

	printf("Enter the elements of the array\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("The Given Array is\n");
	for(int i=0 ;i<size; i++){
		printf("| %d |",arr[i]);
	}

	MergeSort(arr,0,size-1);

	printf("\nEnter the kth element to be searched\n");
	int k;
	scanf("%d",&k);

	printf("The sorted array is\n");
	for(int i=0 ;i<size; i++){
		printf("| %d |",arr[i]);
	}

	printf("The %d largest element is %d\n",k,arr[k-1]);
}
