/*Insertion Sort*/

#include<stdio.h>

void InsertionSort(int arr[],int size){
	for(int i=1 ;i<size;i++){
		int j = i-1;
		int val = arr[i];
		for( ; j >= 0 && arr[j] > val ; j--){
			arr[j+1] = arr[j];
		}

			arr[j+1] = val;
	}
}

void main(){
	int arr[] = {4,-2,7,1,-5,8,5};
	
	InsertionSort(arr,7);
	printf("The Sorted array is\n");
	for(int i=0; i<7 ; i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");
}
