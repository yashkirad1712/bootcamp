/*Sorting the string Array with their length*/
/*Using Bubble Sort*/

#include<stdio.h>
#include<string.h>

int mystrlen(char *ptr){
	int count = 0;

	while(*ptr != '\0'){
		count++;
		ptr++;
	}
	return count;
}

void SortString(char ptr[][10],int num){
	char temp[10];
	for(int i = 0;i <num-1; i++){
		for(int j = 0; j<num-i-1; j++){
			if(strcmp(ptr[j],ptr[j+1])>0){
				strcpy(temp ,ptr[j]);
				strcpy(ptr[j],ptr[j+1]);
				strcpy(ptr[j+1],temp);
			}
		}
	}
}

void main(){
	char str[][10]={"Ashish","Raj","Ravi","YK","Aryan","Shaami"};
	int size = sizeof(str) / sizeof(str[0]);

	SortString(str,size);

	printf("The sorted Array is\n");
	for(int i=0; i<size; i++){
		printf("%s  ",str[i]);
	}
}



