/*Sorting the elements with tneir number of factorials*/

#include<stdio.h>

int Factorial(int val){
	int factorial = 1;
	for(int i=1 ;i <= val;i++){
			factorial = factorial * i;
	}
	return factorial;
}

void Sort(int arr[],int size){
	for(int i=0 ;i<size-1;i++){
		for(int j=0 ;j<size-i-1;j++){
	
			if(Factorial(arr[j]) > Factorial(arr[j+1])){
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void main(){
	printf("Enter the Size of the array\n");
	int size;
	scanf("%d",&size);

	int arr[size];

	printf("Enter the elements of the array\n");
	for(int i=0 ;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("The given array is :\n");

	for(int i=0 ;i<size;i++){
		printf("| %d |",arr[i]);
	}

	Sort(arr,size);
	
	printf("\nThe Sorted array is :\n");

	for(int i=0 ;i<size;i++){
		printf("| %d |",arr[i]);
	}	
	printf("\n");
}

