/*Workingof Bubble sort in single for loop*/

#include<stdio.h>

void BubbleSort(int arr[],int size){

	for(int i=0 ;i<size-2;i++){
		if(arr[i] > arr[i+1]){
			int temp = arr[i];
			arr[i] = arr[i+1];
			arr[i+1] = temp;
			i--;
		}
	}
}

void main(){
	int arr[]={-5,-6,3,1,2};

	int size = sizeof(arr)/sizeof(arr[0]);

	printf("The array is\n");
	for(int i =0 ;i<size ;i++){
		printf("|%d|",arr[i]);
	}
	printf("\n");

	BubbleSort(arr,size);

	printf("The Sorted array is\n");
	for(int i =0 ;i<size ;i++){
		printf("|%d|",arr[i]);
	}
	printf("\n");
}
