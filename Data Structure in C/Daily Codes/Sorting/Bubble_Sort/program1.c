/*Workingof Bubble sort*/
/* here the mostly two for loops are taken and outer loop is for bringing the array bact at start position and inner loop is for bringing th emaximum element at the last of the array*/

/*It works on the last element of the array*/

#include<stdio.h>

void BubbleSort(int arr[],int size){
	for(int i=0 ;i<size-1;i++){
		for(int j=0;j<size-i-1;j++){
			if(arr[j] > arr[j+1]){
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void main(){
	int arr[]={-5,-6,3,5,2};

	int size = sizeof(arr)/sizeof(arr[0]);

	printf("The array is\n");
	for(int i =0 ;i<size ;i++){
		printf("|%d|",arr[i]);
	}
	printf("\n");

	BubbleSort(arr,size);

	printf("The Sorted array is\n");
	for(int i =0 ;i<size ;i++){
		printf("|%d|",arr[i]);
	}
	printf("\n");
}
