/*Power Function in Recursion i.e a^n*/

#include<stdio.h>

int PowerFunction(int n,int a){
	
	if(n == 0){
		return 1;
	}

	return a * PowerFunction(n-1,a);  //Approach one with Time complexity : O(n);
}

int Power_Function(int n,int a){
	
	if(n == 0){
		return 1;
	}
	//Approach two with Time Complexity as Logarithmic ;
	if(n%2 == 1){
		return Power_Function(n/2,a)*Power_Function(n/2,a)*a;
	}else{
		return Power_Function(n/2,a)*Power_Function(n/2,a);
	}
}

void main(){

	int a = 2;
	int n = 5;
	
	printf(" The Power Function Starting \n");
	
	int ret1 = PowerFunction(n,a);
	int ret2 = Power_Function(n,a);

	printf("Approach 1: The Power of %d raised to %d times is:%d\n",a,n,ret1);
	printf("Approach 2: The Power of %d raised to %d times is:%d\n",a,n,ret2);
}
