/*Perfroming operation of print using Recursion*/

#include<stdio.h>

void fun(int x){
	printf("Hello \n");

	if(x != 1){
		fun(--x);
	}
	printf("Bye \n");
}

void main(){
	printf("Enter the Number for starting\n");
	int x;
	scanf("%d",&x);
	fun(x);
}
