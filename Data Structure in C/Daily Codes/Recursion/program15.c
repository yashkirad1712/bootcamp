/*Count zero: number of zero present in the user given Input */

#include<stdio.h>

int countZero(int N){
	if(N == 0){
		return 1;
	}

	if(N < 10){
		return 0;
	}
	
	if(N%10 == 0){
		return 1 + countZero(N/10);
	}
	return countZero(N/10);
}

void main(){
	printf("Enter the number To count the zeroes\n");
	int N;
	scanf("%d",&N);

	int ret = countZero(N);
	printf("The number of zeroes in the number: %d\n ",ret);
}
