/*Binary recursion in character array*/

/*Pallindrome array*/

#include<stdio.h>
#include<stdbool.h>

bool pallindrome(char *ptr,int start,int end){
	if(start < end && *(ptr + start) == *(ptr + end)){
		pallindrome( ptr,++start,--end);
	}else{
		return false;
	}

	return true;
}
void main(){
	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);

	char carr[size];
	printf("Enter the elements of the array\n");
	
	for(int i=0 ; i<size ; i++){
		getchar();
		scanf("%c",&carr[i]);
	}
	int start = 0;
	int end = size-1;
	bool ret = pallindrome(carr,start,end);

	if(ret == true){
		printf("The string is Pallindrome\n");
	}else{
		printf("The string is not a Pallindrome\n");
	}
		
}


