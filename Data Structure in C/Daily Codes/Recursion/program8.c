/*Printing Sum of the elements of the array*/

#include<stdio.h>

int count = 0;

void sumArr(int *ptr,int size){
	static int sum = 0;
	sum = sum + *(ptr);
	count ++;

	if(count != size){
		sumArr((ptr+1),size);
	}else{
		printf("The sum of the elements of the array is:%d\n",sum);
	}

}

void main(){
	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);
	
	int arr[size];

	printf("Enter the elements of the array\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	sumArr(arr,size);
}
