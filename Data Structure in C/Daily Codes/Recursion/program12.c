/*Multiple Recurssion Fibonaci Series*/



/* Non Recursive code 
#include<stdio.h>

 void Fibo(int N){
 	int a = 0;
	int b = 1;
	
	while(N+1){
		printf("%d\n",a);
		int c = a + b;
		a = b;
		b = c;

		N--;
	}
 }
 
void main(){
	Fibo(5);
}
*/

// Recursive Code

#include<stdio.h>

int fibo(int N){
	if(N == 1){
		return 1;
	}
	if(N == 0){
		return 0;
	}

	return fibo(N - 1) + fibo(N - 2);
}

void main(){
	int ret = fibo(5);

	printf("The Fiboacci Number of 5 is : %d\n",ret);
}
