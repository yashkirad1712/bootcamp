/*Factorial of a Number using Recursion*/

#include<stdio.h>

int factorial(int x,int y){
	static int fact = 1;
	fact = fact * x;

	if(x != 2){
		factorial(--x,y);
	}else{
		printf("The factorial of %d is: %d\n",y,fact);
	}
}

void main(){
	printf("Enter the number for finding the Factorial\n");
	int x;
	scanf("%d",&x);
	int y = x;
	factorial(x,y);
}
