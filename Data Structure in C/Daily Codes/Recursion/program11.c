/*Multiple Recursion Basics*/

#include<stdio.h>

int fun(int N){
	printf("The values of N is: %d\n",N);

	if(N <= 1){
		return 1;
	}

	return N + fun(N-1) + fun(N-2);	
}

void main(){
	
	int sum = fun(4);
	printf("The value of Sum is : %d\n",sum);
}
