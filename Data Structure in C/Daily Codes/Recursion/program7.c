/*Printing the Array using Recursion*/

#include<stdio.h>

int count = 0;

void printArr(int *ptr,int size){
	printf("| %d |",*ptr);
	count ++;
	if(count != size){
		printArr((ptr + 1),size);
	}

}

void main(){
	printf("Enter the size of the array: ");
	int size;
	scanf("%d",&size);

	int arr[size];
	printf("\nEnter the elements in the Array\n");
	for(int i =0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printArr(arr,size);
}
