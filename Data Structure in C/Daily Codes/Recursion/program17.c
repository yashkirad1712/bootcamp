/*Two even Numbers are present in the array or not*/

#include<stdio.h>
#include<stdbool.h>

bool twoEven(int *ptr,int size,int count){
	if(size == 0 && count == 2){
		return true;
	}

	if(size == 0 && count != 2){
		return false;
	}

	if(*(ptr + (size-1)) % 2 == 0){
		count++;
	}

	return twoEven(ptr ,(size-1),count);
}

void main(){
	int arr[]={10,3,20,5};
	int count = 0;
	bool ret = twoEven(arr,4,count);

	printf("%c",ret);
}
