/*Count the steps involved in the Execution of the program*/

#include<stdio.h>

int countstep(int N){
		if(N==0){
			return 0;
		}
		if(N%2 == 0){
			return 1 + countstep(N/2);
		}else{
			return 1 + countstep(N-1);
		}
}

void main(){
	printf("Enter the number:\n");
	int N;
	scanf("%d",&N);
	int ret = countstep(N);

	printf("The steps required are %d\n",ret);
}
