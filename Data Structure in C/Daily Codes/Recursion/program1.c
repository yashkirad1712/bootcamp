/*Introduction to recursion without the termination condition*/

#include<stdio.h>

void fun(int x){
	printf("%d\n",x);
	fun(--x);
}

void main(){
	fun(5);
}
