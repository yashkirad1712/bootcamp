/*Number Reverse in Recursion*/

#include<stdio.h>

int reverse(int N,int Num){
	if(N==0){
		return Num;
	}
	return reverse(N/10,Num= Num*10 + N%10);
}

void main(){
	printf("Enter the number:\n");
	int N;
	scanf("%d",&N);
	int ret = reverse (N,0);
	printf("%d\n",ret);
}
