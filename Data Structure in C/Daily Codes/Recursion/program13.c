/*Multiple Recursion  : Sum of Array*/

#include<stdio.h>

int sumArr(int *ptr,int size){
	if(size == 0){
		return 0;
	}

	return *(ptr + (size-1)) + sumArr(ptr,(size-1));
}

void main(){

	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);

	printf("Enter the elements of the array\n");
	
	int arr[size];

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int sum = 0;
	sum = sumArr(arr,size);

	printf("The sum is : %d",sum);

}
