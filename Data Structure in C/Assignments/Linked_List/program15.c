/*WAP that copies elements in ascending order from the source singly linked list to the destination linked list*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;
node *last = NULL;
int flag = 0;
int flag1 = 0;


node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	
	printf("Enter the Data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

int addNode(node **head){
	node *newNode = createNode();
	if(newNode == NULL){
		return -1;
	}else{
		if(*head == NULL){
			*head = newNode;
		}else{
			node *temp = *head;
			while(temp->next != NULL){
				temp = temp->next;
			}
			temp->next = newNode;
		}
		return 0;
	}

}


int printLL(node **head){
	if(*head == NULL ){
		return -1;
	}else{
		node *temp = *head;

		while(temp->next != NULL){
			printf("| %d |->",temp->data);
			temp = temp->next;
		}
			printf("| %d |",temp->data);
		return 0;
	}
}

void copy(){
	node *temp = head1;
	while(temp != NULL){
		node *newNode = (node*)malloc(sizeof(node));
		if(head2 == NULL){
			head2 = newNode;
		}else{
			last->next = newNode;
		}
		last = newNode;
		last->data = temp->data;
		temp = temp->next;
	}

}




int Asc(){
		if( head1 == NULL){
			return -1;
		}else{
			copy();
			node *temp1 = head2;
			node *fast = temp1->next;

 			while(temp1->next != NULL){
				while(fast != NULL){
					int val = 0;
					if(temp1->data > fast->data ){
						val = temp1->data;
						temp1->data = fast->data;
						fast->data = val;
					}
					fast = fast->next;
				}
				fast = temp1->next;
				temp1 = temp1->next;
			}
			return 0;	
		}	
}

void main(){
	char choice;

	do{
	
		printf("|  1. AddNode       |\n");
		printf("|  2. PrintLL       |\n");
		printf("|  3. CopyAsc       |\n");

		int ch;
		printf("Enter your choice : ");
		scanf("%d",&ch);

		printf("\n");
		
		switch(ch){
                        case 1:
				{
					int ret = addNode(&head1);
					if(ret == -1){
						printf("Linked list is FULL\n");
					}else{
						printf("Added succesfully in source list\n");
					}
				}
				break;
			case 2:
				{
					char choice;
					do{
						printf("| 1. Source List      |\n");
						printf("| 2.Destination List  |\n");
	
						printf("Enter your choice: \n");
						int ch;
						scanf("%d",&ch);
	
						switch(ch){
							case 1:
								{
									int ret = printLL(&head1);
									if(ret == -1){
										printf("Linked list is empty\n");
									}
								}
								break;
							case 2:
								{
									if(flag == 1){
										int ret = printLL(&head2);
										if(ret == -1){
											printf("Linked list is empty\n");
										}
									}else{
										printf("Copy & Concatinate the linked list for ,destination list\n ");
									}
								}
								break;
							default:
								printf("Invaliud list choosen to print\n");
								break;	
						}
						getchar();
						printf("\nContinue printing :");
						scanf("%c",&choice);
					}while(choice == 'y' || choice == 'Y');
				}
				break;
			case 3:
				{
					flag = 1;
					int ret = Asc();
	
					if(ret == -1){
						printf("Invalid Conditions\n");
					}else{
						printf("Copy in Ascending Order is successfull\n");
					}
						
				}
				break;

			default :
				printf("Invalid Input\n");
				break;	

			
		}
		getchar();
		printf("Do you want To continue :\n");
		scanf("%c",&choice);

	}while(choice == 'y' || choice == 'Y');

}











































