/*Write a program that concatinates two singly linked list such that a certain range of elements get concat to the destination linked list */


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;
int count = 0;
int flag = 0;

node* createNode(){
	node *newNode =(node*)malloc(sizeof(node));

	printf("Enter the Data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

int addNode(node **head){
	
	node *newNode= createNode();
	if(newNode == NULL){
		return -1;
	}else{
		if(*head == NULL){
			*head = newNode;
		}else{
			node *temp  = *head;
			while(temp->next != NULL){
				temp = temp->next;
			}
			temp->next = newNode;
		}
		return 0;
	}	
}


int printLL(node **head){
	if(*head == NULL){
		return -1;
	}else{
		node *temp = *head;
		while(temp->next != NULL){
			printf("| %d |->",temp->data);
			temp = temp->next;
		}
	
		printf("| %d |",temp->data);
		return 0;
	}	
}

int concatination(int sRange,int eRange){
	if(head1 == NULL || head2 == NULL){
		return -1;
	}else{
		node *temp = head2;
		while(temp->next != NULL){
			temp = temp->next;
		}
		node *temp1 = head1;
		while(temp1->next != NULL){
			if(count >= sRange && count <= eRange ){
				temp->next = temp1;
				temp = temp->next;
			}
			temp1 = temp1->next; 
		}
		temp->next = NULL;
		
		return 0;	
	}	
}

void main(){

	char choice;

	do{
		printf("|1. AddNode         |\n");
		printf("|2. PrintLL         |\n");
		printf("|3. Concatination   |\n");
		

		printf("Enter Your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
		
			case 1:
				{
					char choice;
					do{
						printf("Choose the Linked list\n");
						printf("|1. Source List          |\n");
						printf("|2. Destination list     |\n");
						

						printf("Enter your choice\n");
						int ch;
						scanf("%d",&ch);

						switch(ch){
							case 1:
								{
									if(flag == 0){	
									  int ret = addNode(&head1);
										if(ret == -1){
									 		printf("Linked List is FULL\n");
										}else{
											printf("Successfully added in 1st Linked list\n");
										}
									}else{
										printf("Cant add elements,please re RUN the code\n");
									}	
								}
								break;
							case 2:	
								{
								  int ret = addNode(&head2);
								 	if(ret == -1){
								 		printf("Linked List is FULL\n");
									}else{
										count ++;
										printf("Successfully added in 2nd Linked list\n");
									}
								}
								break;
							
							default :
								printf("Invalid Linked list choosen\n");
								break;	

						}
						getchar();
						printf("Do you want to continue\n");
						scanf("%c",&choice);	
					}while(choice == 'y' || choice == 'Y');
				}
				break;
			case 2:
				
				{
					char choice;
					do{
						printf("Choose the Linked list\n");
						printf("|1. Source List               |\n");
						printf("|2. destination list          |\n");
						printf("|3. Concatinated Linked list  |\n");

						printf("Enter your choice\n");
						int ch;
						scanf("%d",&ch);

						switch(ch){
							case 1:
								{
									if(flag == 0){	
									int ret = printLL(&head1);
										if(ret == -1){
									 		printf("Linked list ONE is empty \n");
										}
									}else{
										printf("Cant print this node after Concatination\n");
									}	
								}
								break;
							case 2:	
								{
								int ret = printLL(&head2);
									if(ret == -1){
								 		printf("Linked List TWO is empty\n");
									}
								}
								break;
							case 3:	
								{
									if(flag == 1){	
										int ret = printLL(&head2);
											if(ret == -1){
								 				printf("Linked List is empty\n");
											}else{
												printf("\nPrinted Concatinated Linked list\n");
											}
									}else{
										printf("Concatinate the linked list First\n");
									}	
								}
								break;


								
							default :
								printf("Invalid Linked list choosen\n");
								break;	

						}
						getchar();
						printf("\n Printing done,Continue print : \n");
						scanf("%c",&choice);	
					}while(choice == 'y' || choice == 'Y');
				}
				break;

			case 3:
				{
					flag = 1;
					int sRange = 0,eRange = 0;
					printf("Enter the Starting Range : ");
					scanf("%d",&sRange);
					printf("\n");
					printf("Enter the Ending Range : ");
					scanf("%d",&eRange);
					printf("\n");
						
					if((sRange <= 0 || sRange >= count) || (eRange <= 0 || eRange >= count)){
						printf("Invalid Ranges \n");
					}else{
						int ret = concatination(sRange,eRange);
						if(ret == -1){
							printf("Invalid Conditions for Concatination\n");
						}else{
							printf("Concatinated\n");
						}
					}	
				}
				break;

			default :
				printf("Invalid Input\n");
				break;
		}
		getchar();
		printf("\n Do you want to continue: ");
		scanf("%c",&choice);	
	}while(choice =='y' || choice == 'Y');
}











