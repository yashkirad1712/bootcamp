/*Write a program that searches all occurences of a particular element from a singly linear linked list*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode =(node*)malloc(sizeof(node));

	printf("Enter the Data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

int addNode(){
	
	node *newNode= createNode();
	if(newNode == NULL){
		return -1;
	}else{
		if(head == NULL){
			head = newNode;
		}else{
			node *temp  = head;
			while(temp->next != NULL){
				temp = temp->next;
			}
			temp->next = newNode;
		}
		return 0;
	}	
}

int occurrence(int num){

	node *temp = head;
	int count = 0;
	while(temp != NULL){
		if(temp->data == num){
			count++;
		}
		temp = temp->next;	
	}
	return count;	
}



int printLL(){
	if(head == NULL){
		return -1;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("| %d |->",temp->data);
			temp = temp->next;
		}
	
		printf("| %d |",temp->data);
		return 0;
	}	
}

void main(){
	char choice;

	do{
		printf("|1. AddNode      |\n");
		printf("|2. PrintLL      |\n");
		printf("|3. Occurrence   |\n");
		//printf("|4. AddNode    |\n")

		printf("Enter Your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
		
			case 1:
				{
					int ret = addNode();
					if(ret == -1){
						printf("Linked list If FULL\n");
					}else{
						printf("Added successfully\n");
					}
				}
				break;
			case 2:
				{
					int ret = printLL();
					if(ret == -1){
						printf("Linked list is Empty\n");
					}
				}
				break;

			case 3:
				{
					printf("Enter the Number to check : ");
					int num;
					scanf("%d",&num);
					if(num <0){
						printf("Invalid Number\n");
					}else{
						int ret = occurrence(num);
						if(ret == 0){
							printf("\nNumber NOT PRESENT");
						}else{
							printf("\n %d is the Count of occurrence\n",ret);
						}
					}	
				}
				break;

			default :
				printf("Invalid Input\n");
				break;
		}
		getchar();
		printf("\n Do you want to continue: ");
		scanf("%c",&choice);
		printf("\n");	
	}while(choice =='y' || choice == 'Y');
}











