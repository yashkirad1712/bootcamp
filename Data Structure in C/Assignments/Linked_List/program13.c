/*WAP to check whether the source linked list is the sublist of the destination linked if yes then return the first position at which the sublist is found*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;
node *last = NULL;
int pos = 0;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	if(newNode == NULL){
		printf("Memory FULL\n");
		exit(0);
	}
	
	printf("Enter the data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

int addNode(node **head){
	node *newNode = createNode();

	if(*head == NULL){
		*head = newNode;
		last = newNode;
	}else{
		last->next = newNode;
		last = newNode;
	}
	return 0;
}

int printLL(node **head){
	if(*head == NULL){
		return -1;
	}else{
		node *temp = *head;
		while(temp->next != NULL){
			printf(" | %d |->" ,temp->data);
			temp = temp->next;
		}
			printf(" | %d |" ,temp->data);
		return 0;
	}
}



int appearence(){
	if(head1 == NULL || head2 == NULL){
		return -1;
	}else{
		node *temp1 = head1;
		node *temp2 = head2;
		node *temp = temp2;

		while(temp2 != NULL){
			pos = pos +1;
			temp = temp2;
			while(temp1 != NULL){
				if(temp1->data == temp->data){
					temp1 = temp1->next;
					temp = temp->next;
				}else{
					break;
				}
			}
			if(temp1 == NULL){
				return pos;
			}
		
			temp1 = head1;
			temp2 = temp2->next;
		}
	}
}

void main(){
	char choice;

	do{
		printf("|  1.AddNode    |\n");
		printf("|  2.PrintLL    |\n");
		printf("|  3.Appearence |\n");

		printf("Enter your Choice : ");
		int ch;
		scanf("%d",&ch);
		printf("\n");

		switch(ch){
			case 1:
				{
					char choice;
					do{
						printf("|1.Source Linked List       |\n");
						printf("|2.Destination Linked list  |\n");
						
						printf("Enter your Choice : ");
						int ch;
						scanf("%d",&ch);

						switch(ch){
							case 1:
								{
							       	int ret = addNode(&head1);
								if(ret == 0){
									printf("Successfully added in source Linked list\n");
									
								}
							       }
							       break;

							case 2:{
							       	int ret = addNode(&head2);
								if(ret == 0){
									printf("Successfully added in Destination Linked list\n");
								}
							       }
							       break;
							 
							default:
							 	printf("Invalid Linked List choosen\n");
							  	break;	
							}
						getchar();
						printf("Do you want to continue Adding: ");
						scanf("%c",&choice);
						printf("\n");
					}while(choice == 'y' || choice == 'Y');

				}
				break;
			case 2:
				{
					char choice;
					do{
						printf("|1.Source Linked List       |\n");
						printf("|2.Destination Linked list  |\n");
						
						printf("Enter your Choice : ");
						int ch;
						scanf("%d",&ch);
						printf("\n");

						switch(ch){
							case 1:{
							       	int ret = printLL(&head1);
								if(ret == -1){
									printf("Source List is Empty\n");
								}
							       }
							       break;

							case 2:{
							       	int ret = printLL(&head2);
								if(ret == -1){
									printf("Destination list is empty\n");
								}
							       }
							       break;							       
							 
							default:
							 	printf("Invalid Linked List choosen\n");
							  	break;	
							}
						getchar();
						printf("\nDo you want to continue Printing: ");
						scanf("%c",&choice);
						printf("\n");
					}while(choice == 'y' || choice == 'Y');

				}
				break;

			case 3:
				{
					int ret = appearence();
					if(ret == -1){
						printf("Source list is not a Sub-List of Destination List\n");
					}else{
						printf("First Sub List found at positon %d \n",ret);
					}
				}
				break;
			default:
				printf("Invalid Input\n");
				break;	
		}
		getchar();
		printf("Do you want to continue : ");
		scanf("%c",&choice);
		printf("\n");
	}while(choice == 'y' || choice == 'Y');
	
}


















