/*Implementation of push pop and peek in stack using Singly Linked list*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;
int flag1 = 0,flag2 = 0;

node * createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	
	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

int countNode(){
	node *temp = head;
	int top=0;
	while(temp != NULL){
		top++;
		temp = temp->next;
	}
	return top;
}

int push(int size){
	node *newNode = createNode();
	int top = countNode();
	if(top == size){
		printf("STACK OVERFLOW\n");
		return -1;
	}else{
		if(head == NULL){
			head = newNode;
		}else{
			node *temp = head;

			while(temp->next != NULL){
				temp = temp->next;
			}
			temp->next = newNode;
		}
		return 0;
	}	
}

int  pop(){
	if(head == NULL){
		printf("STACK UNDERFLOW\n");
		flag1 = 1;
		return -1;
	}else{
		flag2 = 0;
		if(head->next == NULL){
			int rt = head->data;
			free(head);
			head = NULL;
			return rt;
		}else{
			node *temp = head;

			while(temp->next->next != NULL){
				temp = temp->next;
			}
			int rt = temp->next->data;
			free(temp->next);
			temp->next = NULL;
			return rt;
		}
				
	}
}

int peek(){
	if(head == NULL){
		flag2 == 1;
		return -1;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		return temp->data;
	}
}


void main(){
	int size;
	printf("Enter the size of the stack: ");
	scanf("%d",&size);

	char choice;

	do{
		int ch;
		printf("|~~~~~~~~~~~|\n");
	        printf("|1.Push     |\n");
		printf("|2.Pop      |\n");
	        printf("|3.Peek     |\n");
		printf("|~~~~~~~~~~~|\n");

		printf("Enter your Choice: \n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				push(size);
				break;
			case 2:
				{
					if(flag1 == 0){
						int val = pop();
						printf("%d is popped",val);
					}else{
						printf("STACK UNDERFLOW\n");
					}
				}
				break;
			case 3: 
				{
				if(flag2 == 1){
					printf("STACK EMPTY\n");	
				}else{
				int val = peek();
				printf("The value at the last stack is : %d",val);
				}
				}
				break;
			default:
				printf("Invalid Input\n");
		}
		getchar();

		printf("\nDo you want to continue: ");
		scanf("%c",&choice);
		}while(choice == 'y' || choice == 'Y');	
}


