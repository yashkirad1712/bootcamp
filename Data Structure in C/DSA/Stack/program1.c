/*Basic add in Stack Using Array*/

#include<stdio.h>


int top = -1;
int arr[5];

void push(int data){
	arr[++top] = data;
}

void main(){
	for(int i=0;i<5;i++){
	  int data;
	  printf("Enter  the data to push on stack\n");
	  scanf("%d",&data);
	  push(data);
	}

	printf("The stack Frame created is as follows : \n");

	for(int i=top;i>0;i--){
		printf("| %d |->",arr[i]);
	}
	printf("| %d |",arr[0]);
	printf("\n");
}
