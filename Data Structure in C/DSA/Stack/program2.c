/*Implementation of PUSH POOP & PEEK in stack using array*/

#include<stdio.h>

int top = -1;
int flag = 0,size=0;

int push(int stack[],int size){
	if(top == size-1){
		printf("Stack Overflow\n");
		return -1;
	}else{
	printf("Enter the data\n");
        top++;
	scanf("%d",&stack[top]);
	return 0;
	}
}

int pop(int stack[]){
	if(top == -1){
		flag = 1;
		return -1;
	}else{

	int rt = stack[top];
	top--;
	return rt;
	}
}

int peek(int stack[]){
	if(top == -1){
		printf("Stack Empty\n");
		return -1;
	}else{
		return stack[top];
	}
}
void main(){
	printf("Enter the size of the stack\n");
	scanf("%d",&size);

	int stack[size];
	char ch;

	do {
		
		int choice;
		printf("|~~~~~~~~~~~~|\n");
		printf("|1. Push     |\n");
		printf("|2. Pop      |\n");
		printf("|3. Peek     |\n");
		printf("|~~~~~~~~~~~~|\n");

		printf("Enter your choice\n");
		scanf("%d",&choice);

		switch(choice){
			case 1:
				push(stack,size);
				break;
			case 2:
				{
					int val = pop(stack);
					if(flag == 0){
						printf("%d is POPPED\n",val);
					}else{
						printf("STACK UNDERFLOW -- you had popped excess elements\n");
					}
				}
				break;
			case 3:
				{
					int val = peek(stack);
					printf("The value at current stack is: %d\n",val);
				}
				break;	
			default:
				printf("Invalid Input\n");			
		}
		getchar();
		printf("\nDo you want to continue :");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');

}
