/*Real time doubly linked list example*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Laptops{
	struct Laptops *prev;
	int data;
	struct Laptops *next;
}lap;

lap* head=NULL;
lap* createNode(){
	lap* newNode=(lap*)malloc(sizeof(lap));

	printf("Enter the price of laptop\n");
	scanf("%d",&newNode->data);

	newNode->prev=NULL;
	newNode->next=NULL;

	return newNode;
}

int addNode(){
	lap*newNode=createNode();
	if(head == NULL){
		head=newNode;
	}else{
		lap* temp=head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
	return 0;
}	

int addFirst(){
	lap*newNode=createNode();
	if(head == NULL){
		head = newNode;
	}else{
		newNode->next=head;
		head->prev=newNode;
		head=newNode;
	}
	return 0;

}

int countNode(){
	lap*temp=head;
	int count=0;
	while(temp != NULL){
		count++;
		temp=temp->next;
	}
	return count;
}

int addAtPos(int pos){
	int count=countNode();
	if(pos==1){
		addFirst();
	}else if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return -1;
	}else if(pos=count+1){
		addNode();
	}else{
		lap*newNode=createNode();
		lap*temp=head;

		while(pos-2){
			temp=temp->next;
			pos--;
		}
		newNode->next=temp->next;
		temp->next->prev=newNode;
		newNode->prev=temp;
		temp->next=newNode;

	}
	return 0;
}

void delFirst(){
	int count = countNode();
	if(head == NULL){
		printf("LInked List Already empty ,Add Node first\n");
	}else if(count == 1){
		free(head);
		head=NULL;
	}else{
		head = head->next;
		free(head->prev);
		head->prev=NULL;
	}
}
void delLast(){
	lap*temp=head;
	if(head==NULL){
		printf("The linked list is cleaned\n");
	}else{
		while(temp->next->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}

int delatPos(int pos){

	int count = countNode();

	 if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return -1;
	}else if(pos == count){
		delLast();
	}else if(pos == 1){
		delFirst();
	}else{
		lap*temp=head;

		while(pos-2){
			temp=temp->next;
			pos--;
		}
		
		temp->next=temp->next->next;
		free(temp->next->prev);
		temp->next->prev=temp;
	}
	return 0;
}

void printLL(){
	if(head==NULL){
		printf("No Nodes to print\n");
	}else{
		lap*temp=head;
		while(temp->next != NULL){
			printf("|%d |->",temp->data);
			temp=temp->next;
		}
		printf("|%d|",temp->data);
		
	}
}

void main(){
	char choice;

	do{
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		printf("|1.Add Node              |\n");
		printf("|2.Add at First Node     |\n");
		printf("|3.count Node            |\n");
		printf("|4.Add at Position       |\n");
		printf("|5.Delete First Node     |\n");
		printf("|6.Delete last Node      |\n");
		printf("|7.Delete At Position    |\n");
		printf("|8.PrintLL               |\n");
		printf("|~~~~~~~~~~~~~~~~~~~~~~~~\n");
		int ch;

		printf("\nEnter your choice: ");
		scanf("%d",&ch);

		switch(ch){
			case 1: 
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				printf("count = %d\n",countNode());
				break;
			case 4: 
				{
					int pos;
					printf("Enter the psoition to act on\n");
					scanf("%d",&pos);
					addAtPos(pos);
				}
				break;
			case 5: 
				delFirst();
				break;
			case 6:
				delLast();
				break;
			case 7:
				{
					int pos;
					printf("enter the position to delete\n");
					scanf("%d",&pos);
					delatPos(pos);
				}
				break;
			case 8:
				printLL();
				break;
			default:
				printf("enter the valid position,code exit\n");	
		
		}

		getchar();
		printf("\nDo you want to continue:");
		scanf("%c",&choice);
	}while(choice =='y'|| choice=='Y');
}




