/*Doubly linked list (complete cases)*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node* CreateNode(){
	node *NewNode = (node*)malloc(sizeof(node));
	
	printf("Enter the data:\n");
	scanf("%d",&NewNode->data);
	NewNode->prev = NULL;
	NewNode->next = NULL;
	
	return NewNode;
}

node* head = NULL;

int AddNode(){
	node *NewNode = CreateNode();
	
	if(head == NULL){
		head = NewNode;
	}else{
		node *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = NewNode;
		NewNode->prev = temp;
	}
	return 0;	
}

int PrintLL(){
	if(head != NULL){
		node *temp = head;

		while(temp->next != NULL){
			printf("| %d |->",temp->data);
			temp = temp->next;
		}
		printf("| %d |",temp->data);
	}else{
		printf("Linked List is Empty\n");
		return -1;
	}	
	return 0;
}

int CountNode(){
	int count = 0;
	if(head != NULL){
		node *temp = head;
		while(temp != NULL){
			count ++;
			temp = temp->next;
		}
	}

		return count;
}

int AddFirst(){
	node *NewNode = CreateNode();

	if(head == NULL){
		head = NewNode;
	}else{
		head ->prev = NewNode;
		NewNode->next = head;
		head = NewNode;
	}
	return 0;
}

int AddAtPos(int pos){
	
	int count = CountNode();

	if(pos > 0 && pos < count+2){
		node *NewNode = CreateNode();
		if(pos == 1 && head == NULL){
			head = NewNode;
		}else if(pos == 1){
			NewNode->next = head;
			head->prev = NewNode;
			head = NewNode;
		}else if(pos == count +1){
			AddNode();
		}else{
			node *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			NewNode->next = temp->next;
			NewNode->prev = temp;
			temp->next = NewNode;
		}
		return 0;
	}else{
		printf("Invalid Position\n");
		return -1;
	}	
}

int DelFirst(){
	if(head == NULL){
		printf("Linked list is Empty\n");
		return -1;
	}else{
		head = head ->next;
		free(head->prev);
		head->prev = NULL;
	}
	return 0;
}

int DelLast(){
	if(head == NULL){
		printf("Linked List is Empty\n");
		return -1;
	}else{
		node *temp = head;
		while(temp->next->next != NULL){
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
		return 0;
	}
}

int DelAtPos(int pos){
	int count = CountNode();

	if(head == NULL){
		printf("Linked list is Empty\n");
		return -1;
	}else if(pos == 1){
		DelFirst();
	}else if(pos == count){
		DelLast();
	}else{
		node *temp = head;

		while(pos - 2){
			temp = temp->next;
			pos--;
		}
		temp->next = temp->next->next;
		free(temp->next->prev);
		temp->next->prev = temp;
	}
	return 0;
}

int InsertNode(int num){
	if(head == NULL){
		while(num){
			AddNode();
			num--;
		}
	}else{
		node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		while(num){
			node *NewNode = CreateNode();
			temp->next = NewNode;
			temp = NewNode;
			num--;
		}
	}
	return 0;
}

int DeleteNode(){
	int count = CountNode();
	node *temp1 = head;
	node *temp2 = head;
	while(count){
		temp1 = temp2->next;
		free(temp2);
		temp2 = temp1;
		count --;
	}
	head = NULL;
	return 0;
}



void main(){
	printf("The implementation of single liked list\n");

	char choice;

	do{
		printf("~~~~~~~~~~~~~~~~~~~\n");
		printf("| 1.Add Element   |\n");
		printf("| 2.Add First     |\n");
		printf("| 3.Add At Pos.   |\n");
		printf("| 4.Del First     |\n");
		printf("| 5.Del Last      |\n");
		printf("| 6.Del at Pos.   |\n");
		printf("| 7.Print LL      |\n");
		printf("| 8.Count LL      |\n");
		printf("| 9.Special Cases |\n");
		printf("~~~~~~~~~~~~~~~~~~~\n");

		int ch;
		printf("What do you want to choose:\n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1:	{
					AddNode();
					break;
				}
			case 2: {
					AddFirst();
					break;
				}
			case 3:{
				        int pos;
					printf("Enter the position to add on:\n");
					scanf("%d",&pos);
                                    
			       		AddAtPos(pos);
					break;	
			       }
			case 4:{
			       		DelFirst();
					break;
			       }
			case 5:{
			       		DelLast();
					break;
			       }	     
			case 6:{
			       		int pos;
					printf("Enetr the position to delete on:\n");
					scanf("%d",&pos);
					DelAtPos(pos);
					break;
			       }      
			case 7:{
				       printf("The Linked list so formed is:\n");
			       		PrintLL();
					break;
			       }  	
	   		case 8:{	
				        printf("Count of nodes : %d\n ",CountNode());
					break;
			       }
			case 9:{     
				       
				        int val;
			       		printf("choose the option:\n 1.Add certain Nodes \n 2.Delete Certain nodes\n");
					printf("Enter the option : ");
					scanf("%d",&val);
					printf("\n");
					switch(val){
						case 1:{
						       		printf("Enter the number of nodes to add at a time:\n");
								int num;
								scanf("%d",&num);
								InsertNode(num);
								break;
						       }
						case 2:{
						       		printf("Deleting All Nodes...\n");
								DeleteNode();
								break;
						       }
						default:{
								printf("No more Special Cases handeled yet!!!!!\n");
								break;
							}       
					}
				break;	
			       }       
			default :{
				 	printf("Choose the correct response!!Try again.\n");
					break;
				 }		       
		}

		getchar();
		printf("\nDo you want to continue:");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
