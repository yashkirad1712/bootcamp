/*Doubly Circular template*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode =(node*)malloc(sizeof(node));
	newNode->prev=NULL;
	
	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next =NULL;
}

int countNode(){
	if(head == NULL){
		printf("call Addnode functions....");
	}else{
	node *temp =head;
	int count =0;
	
	while(temp->next != head){
		count++;
		temp =temp->next;
	}
	count++;
	return count;
	}
}

void printLL(){
	if(head == NULL){
		printf("No Nodes Found\n");
	}else{
		node *temp = head;
	
		while(temp->next != head){
			printf("| %d |->",temp->data);
			temp=temp->next;
		}
		printf("| %d |",temp->data);	
	}

}

int addNode(){
	node *newNode =createNode();

	if(head == NULL){
		head = newNode;
		head->next=head;
		head->prev=head;
	}else{
		head->prev->next = newNode;
		newNode->prev = head->prev;
		newNode->next = head;
		head->prev = newNode;
	}
	printLL();
	return 0;
}

int addFirst(){
	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
		head->next = head;
		head->prev = head;
	}else{
		newNode->next = head;
		head->prev->next = newNode;
		newNode->prev = head->prev;
		head->prev = newNode;
		head = newNode;
	}
	printLL();
	return 0;
}

int addatPos(int pos){
	int count = countNode();

	if(pos <= 0 || pos > count+1){
		printf("Invalid Position,please try Again\n");
		return -1;
	}else{
		if(pos == 1){
			addFirst();
		}else if(pos == count+1){
			addNode();
		}else{
			node *newNode = createNode();
			node *temp = head;

			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next->prev = newNode;
			temp->next = newNode;
			newNode->prev = temp;
		}
		printLL();
		return 0;
	}
}

int delFirst(){
	if(head == NULL){
		printf("There is Nothing to Delete\n");
	}else{
		if(head -> next == head){
			free(head);
			head = NULL;
		}else{
			head->next->prev = head->prev;
			head = head->next;
			free(head->prev->next);
			head->prev->next = head;
		}
	}
	printLL();
	return 0;
}

int delLast(){
	if(head == NULL){
		printf("Please add a node first\n");
	}else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			head->prev = head->prev->prev;
			free(head->prev->next);
			head->prev->next = head;
		}
	}
	printLL();
	return 0;
}

int delatPos(int pos){
	int count = countNode();

	if(pos <= 0 || pos > count+1){
		printf("Invalid Position,Please try again\n");
		return -1;
	}else{
		if(pos == 1){
			delFirst();
		}else if(pos == count){
			delLast();
		}else{
			node *temp =head;

			while(pos-2){
				temp = temp->next;
				pos--;
			}
			node *del =temp->next;
			temp->next = del->next;
			del->next->prev = temp;
			free(del);
		}
	}
	printLL();
	return 0;
}

void main(){
	char choice;

	do{
		printf("|~~~~~~~~~~~~~~~~~~~~~~|\n");
		printf("|1. CountNode          |\n");
		printf("|2. AddNode            |\n");
		printf("|3. AddFirst()         |\n");
		printf("|4. AddatPos()         |\n");
		printf("|5. DelFirst()         |\n");
		printf("|6. DelLast()          |\n");
		printf("|7. DelatPos()         |\n");
		printf("|~~~~~~~~~~~~~~~~~~~~~~|\n");

		int ch;
		printf("Enter your choice :\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				countNode();
				break;
			case 2:
				addNode();
				break;
			case 3:
				addFirst();
				break;
			case 4:
				{
				int pos;
				printf("Enter the position where to add Node:\n");
				scanf("%d",&pos);
				addatPos(pos);
				}
				break;
			case 5:
				delFirst();
				break;
			case 6:
				delLast();
				break;
			case 7:
				{
					int pos;
					printf("Enter the position from where to delete:\n");
					scanf("%d",&pos);
					delatPos(pos);
				}
				break;
			default:
				printf("Sorry but you entered wrong input\n");
		}
		getchar();
		printf("\nDo you Want to continue\n");
		scanf("%c",&choice);
	}while(choice == 'y'|| choice == 'Y' );
}



