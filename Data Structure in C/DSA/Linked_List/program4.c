/*Implementation of singly Linked list Inplace reverse */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;

node * createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

int addNode(){
	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
	return 0;
}

int printLL(){
	if(head == NULL){
		printf("Enter the Nodes before printing\n");
		return -1;
	}else{
	node *temp = head;

	while(temp->next != NULL){
		printf("| %d |->",temp->data);
		temp = temp->next;
	}
	printf("| %d |",temp->data);
	return 0;
	}

}


int IPRSLL(){
	if(head == NULL){
		printf("Inplace reverse is NOT POSSIBLE\n");
		return -1;
	}else{
		if(head->next == NULL){
			printf("Not an IDEAL condition for reversing the Linked List\n");
			return -2;
		}else{
			node *prev = NULL;
			node *next =NULL;

			while(head->next != NULL){
				next = head->next;
				head -> next = prev;
				prev = head;
				head = next;
			}
			head -> next = prev;
			return 0;
		}
	}
}

void main(){
	int nodeCount;

	printf("Enter the number of Nodes you want in Linked list\n");
	scanf("%d",&nodeCount);

	for(int i=1;i<=nodeCount;i++){
		addNode();
	}

	printf("The Linked List Formed is :\n");
	printLL();
	
	printf("\n");

	printf("After Inplace Reverse the Linked list is : \n");
	IPRSLL();
	printLL();
	
	printf("\n");
}
























