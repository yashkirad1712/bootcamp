/*Singly circular linked list template*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node* next;

}node;

node* head=NULL;

node* createNode(){
	node* newNode=(node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;
	return newNode;
}

int addNode(){
	node *newNode=createNode();
	
	if(head == NULL){
		head=newNode;
		newNode->next=head;
	}else{
		node *temp=head;

		while(temp->next != head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
	}
	return 0;
}

int addFirst(){
	node*newNode=createNode();
	if(head == NULL){
		head=newNode;
		newNode->next=head;
	}else{
		newNode->next=head;
		node*temp=head;
		while(temp->next != head){
			temp=temp->next;
		}
		head=newNode;
		temp->next=head;
	}
	return 0;
}

int countNode(){
	        if(head == NULL){
			printf("No nodes to count\n");
		}else{
			node *temp =head;
			int count=0;
			while(temp->next != head ){
				count++;
				temp=temp->next;
			}
			count++;
			return count;
		}
}


int addatPos(int pos){
	int count = countNode();

	if(pos <= 0 || pos > count+1){
		printf("Invalid Position,Correct yourself\n");
	}else{
		if(pos == 1){
			addFirst();
		}else if(pos == count+1){
			addNode();
		}else{
			node*newNode=createNode();

			node* temp=head;


			while(pos -2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
		return 0;
	}
	
}

int delFirst(){
	if(head == NULL){
		printf("Please add Nodes First\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head=NULL;
		}else{
		node*temp=head;
		while(temp->next != head){
			temp =temp->next;
		}
		head=head->next;
		free(temp->next);
		temp->next=head;
		}
		return 0;
	}
	
}

int delLast(){
	if(head == NULL){
		printf("Your Linked list Is totally Empty\n");
		return -1;
	}else{
		node*temp=head;
		while(temp->next->next != head){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=head;
		return 0;
	}
}

int delatPos(int pos){
	int count=countNode();
		
		if(head == NULL){
			printf("No Node to delete\n");
		}else if (pos<=0 || pos>count){
		printf("Please add nodes to delete\n");
		return -1;
		}else if(pos==1){
			delFirst();
		}else if(pos == count){
			delLast();
		}else{
			node*temp=head;

			while(pos-2){
				temp=temp->next;
				pos--;
			}
			node*del=temp;
			temp->next=del->next;
			free(del);
		}
		return 0;
	}

void printLL(){

	if(head == NULL){
		printf("Please Insert data to print\n");
	}else{
	node*temp=head;
	while(temp->next != head){
		printf("| %d|->",temp->data);
		temp=temp->next;
	}
	printf("| %d|",temp->data);
	}
}

void main(){
	char choice;

	do{
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		printf("|1.addNode()                 |\n");
		printf("|2.addFirst()                |\n");
		printf("|3.addatPos()                |\n");
		printf("|4.countNode()               |\n");
		printf("|5.delFirst()                |\n");
		printf("|6.delLast()                 |\n");
		printf("|7.delatPos()                |\n");
		printf("|8.printLL()                 |\n");
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

		int ch;
		printf("Enter Your choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				{
					int pos;
					printf("Enter the position to add on\n");
					scanf("%d",&pos);
					addatPos(pos);
				}
				break;
			case 4:
				printf("Count of Nodes is : %d\n",countNode());
				break;
			case 5:
				delFirst();
				break;
			case 6:
				delLast();
				break;
			case 7:
				{
					int pos;
					printf("Enter the position to delete the node\n");
					scanf("%d",&pos);
					delatPos(pos);
				}
				break;
			case 8:
				printLL();
				break;
			default:
				printf("Please Try again\n");	
		}
		getchar();
		printf("\nDo you want to Continue:.");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}







