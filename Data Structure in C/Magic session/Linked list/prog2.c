#include<stdio.h>
#include<stdlib.h>

typedef struct state{
    char sName[20];
    float budget;
    float lr;
    int pop;
    struct state* next;
}st;

st* head=NULL;

void addNode(){
    st*newNode=(st*)malloc(sizeof(st));

    printf("Enter the name of state\n");
    fgets(newNode->sName,15,stdin);

    printf("Enter the budget\n");
    scanf("%f ",&newNode->budget);

    printf("Enter the literacy rate of the state\n");
    scanf("%f" ,&newNode->lr);

    printf("enter the population of the state\n");
    scanf("%d ",&newNode->pop);

    newNode->next=NULL;

    if(head == NULL){
        head =newNode;
    }else{
        st* temp=head;
        while(temp->next!= NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void PrintLL(){
    st*temp=head;

    while(temp != NULL){
        printf("%s ",temp->sName);
        printf("%f",temp->budget);
        printf("%f",temp->lr);
        printf("%d",temp->pop);

        temp=temp->next;
    }
}


void main(){
    addNode();
    addNode();
    addNode();

    PrintLL();


}
