#include<stdio.h>
#include<stdlib.h>

typedef struct malls{
    char mName[20];
    int num;
    float rev;
    struct malls* next;
}M;

M*head = NULL;

void addNode(){
    M* newNode=(M*)malloc(sizeof(M));

    printf("Enter mall name\n");
    fgets(newNode->mName,15,stdin);
    getchar();
    printf("Enter number of people\n");
    scanf("%d ",&newNode->num);

    printf("Enter the revenue\n");
    scanf("%d",&newNode->rev);

    newNode->next=NULL;

    if(head==NULL){
        head=newNode;
    }else{
        M*temp=head;
        while(temp->next != NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void PrintLL(){
    M*temp=head;

    while(temp != NULL){
        printf("| %s ",temp->mName);
        printf("| %d | ",temp->num);
        printf("| %f ",temp->rev);
    }
}

void main(){
    addNode();
    addNode();
    addNode();

    PrintLL();
}
