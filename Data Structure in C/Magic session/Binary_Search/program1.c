/*Given A sorted array with distinct elements .
  Find the floor of no. K in the array.
  floor => greates element <=K*/

#include<stdio.h>
#include<math.h>

int flr(int *ptr,int size,int key){
	int start = 0,end = size-1,mid = 0,store = 0;

	while(start <= end){
		mid = (start+end) / 2;

		if(*(ptr+mid) == key){
			return *(ptr+mid);
		}

		if(*(ptr+mid) > key){
			end = mid - 1;
		}

		if(*(ptr+mid) < key){
			store = *(ptr+mid);
			start = mid + 1;
		}
	}
	return store;
}

void main(){
	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);

	int arr[size];
	
	printf("Enter the elements of the array\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Enter the key to search\n");
	int key;
	scanf("%d",&key);

	int ret = flr(arr,size,key);

	printf("The floor value of the given key value is: %d\n",ret);
}
