/*Creation of Singly Linked list and Entering the data*/


#include<iostream>

typedef struct Node{
	
	int data;
	struct Node *next;
}Node;

class LinkedList{
	private:
		Node *head ;

	public:
		LinkedList(){

			head = nullptr;
		}

		Node* CreateNode(){
			
			Node* newNode = new Node;

			std::cout <<"Enter the data in The Node"<<std::endl;
			std::cin>> newNode->data;

			newNode->next = nullptr;
			return newNode;
		}

		void AddNode(){
		
			if(head == nullptr){
				head = CreateNode();
			}else{
				
				Node *newNode = CreateNode();
				Node *temp = head;

				while(temp->next != nullptr){
					temp = temp->next;
				}
				temp->next = newNode;
			}
		}

		void AddAtPos(int pos){
			
			//min and max conditions handled here
			if(pos == 1){
				Node *newNode = CreateNode();
				newNode->next = head;
				head = newNode;	
			}else{
				Node *temp = head;
				while(temp != nullptr && pos-2 != 0){
					temp = temp->next;
					pos--;
				}
	
				Node *newNode = CreateNode();
				newNode ->next = temp->next;
				temp->next = newNode;
			}
		}

		void PrintLL(){
			Node* temp = head;

			while(temp->next != nullptr){
				
				std::cout << "| " << temp->data << " |->" ;
				temp = temp->next;
			}

				std::cout << "| " << temp->data << " |" ;
		}
};

int main(){
	
	LinkedList obj1;
	obj1.AddNode();
	obj1.AddNode();
	obj1.AddNode();
	obj1.AddAtPos(2);
	obj1.AddAtPos(6);
	obj1.PrintLL();
	

	return 0;
}


