#include <iostream>
#include <vector>

using namespace std;

// Function to calculate PageRank scores
vector<double> calculatePageRank(const vector<vector<int>>& graph, double dampingFactor, int iterations) {
    int n = graph.size();
    vector<double> pageRank(n, 1.0 / n);

    // Iteratively calculate PageRank
    for (int iter = 0; iter < iterations; ++iter) {
        vector<double> newPageRank(n, 0.0);

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (graph[j][i] == 1) {
                    newPageRank[i] += pageRank[j] / static_cast<double>(graph[j].size());
                }
            }
        }

        for (int i = 0; i < n; ++i) {
            newPageRank[i] = (1.0 - dampingFactor) / n + dampingFactor * newPageRank[i];
        }

        pageRank = newPageRank;
    }

    return pageRank;
}

int main() {
    // Example graph representing web pages and their links
    vector<vector<int>> graph = {
        {0, 1, 0, 1},
        {1, 0, 0, 1},
        {1, 1, 1, 0},
        {0, 0, 1, 0}
    };

    double dampingFactor = 0.85;
    int iterations = 10;

    vector<double> pageRank = calculatePageRank(graph, dampingFactor, iterations);

    // Display PageRank scores
    for (int i = 0; i < pageRank.size(); ++i) {
        cout << "Page " << i << ": " << pageRank[i] << endl;
    }

    return 0;
}

