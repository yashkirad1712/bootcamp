/*Function like macro and object like macro */

#include<iostream>

//Defining the macros: it replaces the function or the variable with the values assigned to them at preprocessing time

#define z 1712           //--->Object Like Macro
#define sum(x,y) x+y     //--->Function Like Macro

int main(){

	int x = 10;
	int y = 20;

	std::cout<<z<<std::endl;
	std::cout<<su(x,y)<<std::endl;

	return 0;
}
