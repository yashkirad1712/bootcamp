/*Variable initialization is of 3 types*/

#include<iostream>

int main(){
	
	int x = 10 ; //Copy Initialization
	
	int y(20); //Direct Initialization
	
	int z{30}; //Uniform Initialization | List initialization
		   //
	
	std::cout << x << std :: endl << y << std::endl << z << std::endl;

	return 0;
}
