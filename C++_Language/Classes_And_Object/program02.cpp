/*Using access specifiers in the class and trying to acess them to learn the errors*/


#include<iostream>

class Demo{
	
	//private:
	public:
		int x = 10;

	public:
		int y = 20;
	
	//protected:
		int z = 30;

};

int main(){
	
	Demo obj;
	std :: cout << obj.x << obj.y << obj.z << std::endl;

	return 0;	
}
