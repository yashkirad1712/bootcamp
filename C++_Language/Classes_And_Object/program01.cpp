/*In Cpp by default everything present in the class is private as nothing can be accessed out of it*/

#include<iostream>

class Company{
	
	int CountEmp = 5000;
	std::string Name = "IBM";

	public:

	void CompanyInfo(){
		
		std::cout << CountEmp <<std::endl;
		std::cout << Name <<std::endl;
	}
};

class Employee{
	
	int Empid = 10;
	float empSal = 95.00f;

	public:
	void EmployeeInfo(){
		
		Company obj;
		std::cout << Empid <<std::endl;
		std::cout << empSal <<std::endl;
		obj.CompanyInfo();
	}
};

int main(){
	Employee *emp1= new Employee();
	emp1->EmployeeInfo();
	return 0;
}
