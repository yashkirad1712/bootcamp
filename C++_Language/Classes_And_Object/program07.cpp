/*not initializing the refrence variable*/

#include<iostream>

int main(){
	
	int x = 10;
	int &y = x ;
	int *ptr;
	int &z;
	std::cout << x << std::endl << y << std::endl << *ptr << std::endl << z << std::endl;
}

/*Here when we try to initialize the pointer ptr withou asssigning it to the value it returns a garbage Value*/
/*Similarly we cannot initiualize the refrence without assigning to any value it gives an error*/
/*In function ‘int main()’:
program07.cpp:10:14: error: ‘z’ declared as reference but not initialized
   10 |         int &z;
      |              ^
      
*/
