/*Refrence variable : it is the alias of an variable 
 * we have to initialize the refrence variable when declaring
 * it is internally pointer to that variable compiler itself puts & while taking and *while accessing*/

#include<iostream>

int main(){
	
	int x = 10;
	int &y = x;  //Refrence Variable
		     //
	std::cout << x <<std::endl << y <<std::endl;

}
