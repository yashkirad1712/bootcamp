/*Variable Shadowing*/

//1. GLobal Level Shadowing
//

#include<iostream>

int x = 20;

int main(){
	
	int x = 10;
	int y = 20;
	{
		int y = 30;
		std::cout << ::x << std::endl << x << y <<std::endl << ::y << std::endl;
		//here ::x refers to the global x and only x refere to the local x and is valid
		//
		//but in case of y whic local we cannot use the ::y to access the 20 in place 30 and is not valid
		x = 30;
	}

	std :: cout << x << std::endl;
}
