/*Creating a structure and a nested one for it*/

#include<iostream>


typedef struct PlayerInfo{
	
	std::string name;
	std::string team;
	int jerNo;
}Player;

typedef struct IPL{
	
	int num ;
	struct PlayerInfo obj;
}IPL;

int main(){
	
	IPL team1;
	std::cout<<"Enter the Number of teams in IPL"<<std::endl;
	std::cin>>team1.num;
	
	std::cout<<"Enter the Captains Name"<<std::endl;
	std::cin>> team1.obj.name;	
	std::cout<<"Enter the Team Name"<<std::endl;
	std::cin>> team1.obj.team;	
	std::cout<<"Enter the Captains Jersey Number"<<std::endl;
	std::cin>> team1.obj.jerNo;	
	
	std::cout<<"-----------------------------------------------"<<std::endl;

	IPL team2;
	std::cout<<"Enter the Captains Name"<<std::endl;
	std::cin>> team2.obj.name;
	getchar();	
	std::cout<<"Enter the Team Name"<<std::endl;
	std::cin>> team2.obj.team;	
	std::cout<<"Enter the Captains Jersey Number"<<std::endl;
	std::cin>> team2.obj.jerNo;	
	

	std::cout<<"-----------------------------------------------"<<std::endl;

	std::cout<<"Team Name: " <<team1.obj.team <<std::endl<<"Captains Name: " << team1.obj.name <<std::endl<<"Captains Jersey Number: "<<team1.obj.jerNo<<std::endl;
	std::cout<<"Team Name: " <<team2.obj.team <<std::endl<<"Captains Name: " << team2.obj.name <<std::endl<<"Captains Jersey Number: "<<team2.obj.jerNo<<std::endl;

	return 0;
}
