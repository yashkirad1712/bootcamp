/*Craeting a basic structure in cpp*/

#include<iostream>

typedef struct IdentityCard{
	
	int id = 10;
	std::string name = "Yash";
	float weight = 98.6;
}Id;

int main(){
			
	Id student1;
       	Id student2;
	Id student3;
	
	std::cout<<" ------------------------------------------------------------------"<<std::endl;	
	//using default values for the student1
	std::cout<<"Student 1: "<<student1.name << std::endl << "Roll number: " << student1.id << std::endl << "Weight: " << student1.weight <<std::endl;	
	
	std::cout<<" ------------------------------------------------------------------"<<std::endl;	
	//changing data for student2
	student2.name = "Aryan";
	student2.id = 20;
	student2.weight = 74.63;
	std::cout<<"Student 2: "<<student2.name << std::endl << "Roll number: " << student2.id << std::endl << "Weight: " << student2.weight <<std::endl;	
	std::cout<<" ------------------------------------------------------------------"<<std::endl;	
	//chnaging few AND KEEPING REST DATA
	student3.name = "Sarthak";
	student3.id = 30;	
	std::cout<<"Student 3: "<<student3.name << std::endl << "Roll number: " << student3.id << std::endl << "Weight: " << student3.weight <<std::endl;	

	return 0;
}
