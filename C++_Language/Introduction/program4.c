/*Linked list BAsic*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Node{

	char name[20];
	char Author[20];
	struct Node *next;
}Node;

Node* head = NULL;

Node* CreateNode(){
	
	Node * newNode = malloc(sizeof(Node));


	printf("Enter the Name\n");
	strcpy(newNode->name ,"C");
	
	printf("Enter the Author Name\n");
	strcpy(newNode->Author , "Yash");

	return newNode;
}

void addNode(){
	
	if(head == NULL){
		
		head = CreateNode();
	}else{

	Node *temp = head;

	while(temp->next != NULL){
		
		temp = temp->next;
	}

	temp->next = CreateNode();
	}
}

void printNode(){
	
	Node *temp = head;

	while(temp != NULL){
		printf("%s\n",temp->name);
		printf("%s\n",temp->Author);

		temp = temp->next;
	}
}
void main(){
	
	printf("Enter the First Author Name\n");
	addNode();
	printf("Enter the Second Author Name\n");
	addNode();
	printf("Enter the Third Author Name\n");
	addNode();

	printNode();
}
