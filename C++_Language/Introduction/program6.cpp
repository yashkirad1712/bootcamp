/*constant data and pointer in Cpp*/

#include<iostream>


int main(){
	
	int y = 20;	
	int x = 10;

	//making Pointer Constant
	int * const ptr = &x;
	ptr = &y;

	
	//making Data Constant
	int const * cptr = &x;
	*cptr = 50;

}
