/*Linked list practice*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Node{
	
	int data;
	//String Name;
	struct Node *next;
}Node;

struct Node *head = NULL;

Node* createNode(){
	
	Node* newNode = malloc(sizeof(Node));
	
	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	return newNode;
}

void printNode(){
	
	while(head != NULL){
		
		printf("%d\n",head->data);

		head = head->next;
	}
}

void main(){
	
	printf("Creating an node\n");

	head = createNode();

	printNode();
}
