import java.util.Arrays;

class DistinctPermutations {
    public static void main(String[] args) {
        int[] nums = {1, 2, 2};
        int size = nums.length;

        System.out.println("Distinct permutations:");
        printDistinctPermutations(nums, size);
    }

    public static void printDistinctPermutations(int[] nums, int size) {
        // Sort the array first
        Arrays.sort(nums);

        do {
            for (int i = 0; i < size; i++) {
                System.out.print(nums[i] + " ");
            }
            System.out.println();
        } while (nextPermutation(nums));
    }

    public static boolean nextPermutation(int[] nums) {
        int i = nums.length - 2;
        while (i >= 0 && nums[i] >= nums[i + 1]) {
            i--;
        }

        if (i < 0) {
            return false;
        }

        int j = nums.length - 1;
        while (nums[i] >= nums[j]) {
            j--;
        }

        swap(nums, i, j);

        int left = i + 1;
        int right = nums.length - 1;

        while (left < right) {
            swap(nums, left, right);
            left++;
            right--;
        }

        return true;
    }

    public static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}

