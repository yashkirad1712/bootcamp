/*Templates in Singly Linked List*/



#include <iostream>

template <typename T>
class Node {
public:
    T data;
    Node<T>* next;

    Node(T val) : data(val), next(nullptr) {}
};

template<typename T>
class LinkedList {
public:
    Node<T>* head;

    LinkedList() : head(nullptr) {}

    void addNode() {
        std::cout << "Enter the data: ";
        T val;
        std::cin >> val;
        Node<T>* newNode = new Node<T>(val);

        if (head == nullptr)
            head = newNode;
        else {
            Node<T>* temp = head;
            while (temp->next != nullptr) {
                temp = temp->next;
            }
            temp->next = newNode;
        }
    }

    void printNode() {
        Node<T>* temp = head;
        while (temp != nullptr) {
            std::cout << temp->data << std::endl;
            temp = temp->next;
        }
    }
};

int main() {
    LinkedList<int> obj;
    obj.addNode();
    obj.addNode();
    obj.addNode();
    obj.addNode();
    obj.addNode();

    obj.printNode();

    return 0;
}

