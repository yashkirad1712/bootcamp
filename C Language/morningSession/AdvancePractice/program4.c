/*Reverse the given array*/

#include<stdio.h>

void reverseArr(int arr[],int size){
	
	int end = size-1;
	int start = 0;

	while(arr[start] < arr[end]){
		int temp =arr[start];
		arr[start] = arr[end];
		arr[end] = temp;
		start++;
		end--;

	}

	
}

void main(){
	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);
	
	int arr[size];

	printf("enter the elements of the array\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	reverseArr(arr,size);

	printf("The reversed array is\n");
	for(int i=0 ;i<size;i++){
		printf("| %d |",arr[i]);
	}
}
