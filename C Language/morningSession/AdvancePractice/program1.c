/*Count the number of elements who has atleast one element greater than itself*/

#include <stdio.h>

int greaterele(int arr[],int size){
	int count =0;

	for(int i=0;i<size;i++){
		for(int j =0;j<size;j++){
			if(arr[i] < arr[j]){
				count++;
				break;
			}
		}
	}
	
	return count;
}

void main(){
	printf("Enter the size of the array\n");
	int size;
	scanf("%d",&size);

	int arr[size];

	printf("Enter the elements of the array\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	int ret = greaterele(arr,size);
	printf("The Count of numbers greater are:%d\n ",ret);
}
