/*Rverse the given array within a certain range*/

#include<stdio.h>

void reverse(int arr[],int B,int C){

	while(B < C){
		int temp = arr[B];
		arr[B] = arr[C];
		arr[C] = temp;
		B++;
		C--;
	}
}

void main(){
	int arr[]={2,5,6};

	printf("Enter the range of the elements to be reversed\n");
	int B,C;
	scanf("%d %d",&B,&C);

	reverse(arr,B,C);

	printf("The reversed array is\n");
	for(int i=0 ; i<size; i++){
		printf(" | %d |",arr[i]);
	}
	printf("\n");
}
