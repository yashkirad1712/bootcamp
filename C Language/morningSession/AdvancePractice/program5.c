/*Array rotation i.e bringing th elast element to the frontside for n number of times*/

#include<stdio.h>

void rotateArr(int arr[],int size,int n){
	int val  = size-1;
	int end =0;
	while(n){
		end = val;
		for(int i=0 ;i<size;i++){
			int temp = arr[end];
			arr[end] = arr[i];
			end--;
		}	
	n--;
	}
}

void main(){
	int arr[]={1,2,3,4};
	int size = sizeof(arr)/sizeof(arr[0]);

	printf("Enter the number of rotations\n");
	int n;
	scanf("%d",&n);
	
	rotateArr(arr,size,n);

	printf("the rotated array is\n");
	for(int i =0 ;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");
}
