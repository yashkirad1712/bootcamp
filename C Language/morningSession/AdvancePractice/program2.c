/*Good Pair
  a pair(i,j) is said to be good pair when i!=j & a[i] +a[j] = B,
  where a is the array & B is the given input of a number*/

#include<stdio.h>

int goodpair(int *ptr,int size,int B){
	int pair = 0;
	int j = 1;

	for(int i =0; i<size ;i++){
		if( i!=j && (*(ptr+i) + *(ptr+j) )== B){
			pair++;
		}
		j++;
	}
	return pair;
}

void main(){
	printf("Enter the size of  the array\n");
	int size;
	scanf("%d",&size);

	int arr[size];
	
	printf("Enter the elements of the array\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	
	printf("Enter the value of B\n");
	int B;
	scanf("%d",&B);

	int ret = goodpair(arr,size,B);

	printf("The number of good pairs are: %d\n",ret);
}
