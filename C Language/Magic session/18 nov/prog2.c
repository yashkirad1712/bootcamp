/*Memory allocation in heap section as a array and printing the 1-D array using malloc*/



#include<stdio.h>
#include<stdlib.h>

void main(){
    int x=5;

    int*ptr=(int*)malloc(sizeof(int));

    for(int i=0;i<5;i++){
        scanf("%d",ptr+i);
    }
    printf("The array is :\n");
     for(int i=0;i<5;i++){
        printf("%d\n",*(ptr+i));
    }
}
