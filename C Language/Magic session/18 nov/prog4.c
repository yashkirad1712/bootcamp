/*Wap that prints the 3D array  using malloc*/

#include<stdio.h>
#include<stdlib.h>

void main(){
    printf("Enter the count of plane ,row & columns\n");
    int pl,row,col;

    scanf("%d %d %d",&pl,&row,&col);

    int *ptr=(int*)malloc(sizeof(int)*pl*row*col);

    printf("Enter the elements in the array\n");
    for(int i=0;i<pl;i++){
        for(int j=0;j<row;j++){
            for(int k=0;k<col;k++){
                scanf("%d ",ptr+(pl*i)+(j*row)+k);
            }
        }
    }

    printf("The array is\n");
    for(int i=0;i<pl;i++){
        for(int j=0;j<row;j++){
            for(int k=0;k<col;k++){
                printf("%d ",*(ptr+(pl*i)+(j*row)+k));
            }
            printf("\n");
        }
    }
}
