/*Memory allocation in the heap section and printing the 2-D array using malloc*/

#include<stdio.h>
#include<stdlib.h>

void main(){
    int row,col;
    printf("Enter the row and column count\n");
    scanf("%d %d",&row,&col);

    int*ptr=(int*)malloc(sizeof(int)*row*col);

    printf("Enter the elements in the array\n");
    for(int i=0;i<row;i++){
        for(int j=0;j<col;j++){
             scanf("%d",ptr +(row*i)+j);
        }
    }

    for(int i=0;i<row;i++){
        for(int j=0;j<col;j++){
            printf("%d ",*(ptr +(row*i)+j));
        }
        printf("\n");
    }
}
