/*printing the index of the string given by the user at th erun time*/
/*checking the term constant string*/

#include<stdio.h>

void main(){
	char carr[] = {'c','o','r','e','2','w','e','b'};

	char *str = "core2web";

	//complete string is printed
	printf("%s\n",carr);
	printf("%s\n",str);

	//printing the nth index of the string
	printf("%c\n",carr[3]);
	printf("%c\n",*(str+3));

	//char *str is constant string so we cannot change the value in it otherwise we will get the segmentation fault
	*str = 'y';
	//constant string goes in read only data so cannot be changed
	printf("%c\n",*str);
	//it will give segmentation fault at run time
	

}
