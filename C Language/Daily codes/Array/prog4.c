/*printing the sum of digits of the array*/

#include<stdio.h>

void main(){
    int arr[4],sum=0,dig=0,n;

    printf("Enter the elements of the array\n");

    for(int i=0;i<4;i++){
        scanf("%d",arr[i]);
    }

    printf("The sum of the digits of the array elements are\n");
    for(int i=0;i<4;i++){

        sum=sum+arr[i];

    }
    printf("the sum of the digits are %d",sum);
}
