/*passing an array to an array*/
#include <stdio.h>

void main(){
    int arr1[]={1,2,3,4,5};
    int arr2[5];

    printf("The passing is done here\n");

    for(int i=0;i<5;i++){
        arr2[i]=arr1[i];

        printf("%d ",arr1[i]);

        printf("%d ",arr2[i]);
    }
    /*direct accessing is done for single value */
    arr1[1]=arr2[2];
    printf("\n");
    printf("%d",arr2[2]);
}
