/*Prefix Array Sum in same array*/

#include<stdio.h>

void PrefixSum(int arr[],int size){
	int temp=0;

	for(int i=1;i<size;i++){
		temp = arr[i-1];
		arr[i] = temp + arr[i];
	}
}

void main(){
	int arr[] ={2,3,4,5,6};
	int size =5;
	printf("The given Array is\n");
	for(int i=0 ;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");

	PrefixSum(arr,size);
	printf("The given Array after formed is\n");
	for(int i=0 ;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");
}
