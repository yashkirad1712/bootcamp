/*Array Equillibrium */

/*Here the sum of element of array less then key is equal to the element next to key*/

#include<stdio.h>

int Equillibrium(int arr[],int size,int key){
	if(key < 0 || key > size-1){
		return -1;
	}else{
		int start =0;
		int end = size - 1;
		int sum1=0,sum2=0;

		if(key-1 < 0){
			sum1 = arr[0];
		}else{
			for(int i=0;i<key;i++){
				sum1 = sum1 + arr[i];
			}
		}

		if(key+1 > end){
			sum2 = arr[end];
		}else{
			for(int j=key+1;j<=end;j++){
				sum2 = sum2 + arr[j];
			}
		}

		if(sum1 == sum2){
			return 1;
		}	
	}
	return -1;
}

void main(){
	int arr[] = {-7,1,5,2,-4,3,0};

	printf("Enter the key element\n");
	int key;
	scanf("%d",&key);

	int ret = Equillibrium(arr,7,key);

	if(ret == -1){
		printf("Array is not Equillibrium\n");
	}else{
		printf("Array is in Equllibrium\n");
	}
}
