/*Sub Array sum of single sub array*/

#include<stdio.h>

void main(){
	int arr[]={4,2,1,3};
	int N = 4;

	printf("The broot Force approach for sub array printing with Sum of sub arrays\n");

	for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
			int sum = 0,k;
			for(k=i;k<=j;k++){
				sum = sum + arr[k];
			}
			if(sum != 0){
				printf("Single sub array Sum is:%d\n",sum);
			}	
		}
	}
}
