/*Sub Array :
 PRint all the sub Array of the given Array
 i.e arr[]={4,2,1,3}
 Sub array ={4},{4,2},{4,2,1},{4,2,1,3},{2},{2,1},{2,1,3},{1},{1,3},{3}
 
 formula to find the sub array count = (n*(n+1))/2  */


#include<stdio.h>

void main(){
	int arr[] ={4,2,1,3};

	printf("Brute Force Approach to print the Sub array is:\n");
	
	int N = 4;
	for(int i=0;i<N;i++){
		for(int j=0 ;j<N;j++){
			for(int k=i;k<=j;k++){
				printf("| %d |",arr[k]);
			}
			printf("\n");
		}
		
	}
}
