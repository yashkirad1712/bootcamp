/*Sum of Every Single Sub Array*/

#include<stdio.h>

void SubArrSum(int arr[],int N){
	int Sum = 0;
	for(int i =0 ;i<N;i++){
		int val =0;
		for(int j=i;j<N;j++){
			val = val + arr[j];
			printf("The Sum of single Sub array is:%d\n",val);
			Sum = Sum + val;	
		}
	}
	printf("The sum of all The Sub arrays is:%d\n",Sum);
}

void main(){
	int arr[] ={2,4,1,3};
	SubArrSum(arr,4);
}
