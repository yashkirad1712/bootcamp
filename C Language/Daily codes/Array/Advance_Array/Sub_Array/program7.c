/*Kadane's Algorithm*/
/*If sum <0, the sum = 0*/

#include<stdio.h>

void Sum(int arr[],int N){
	int sum =0,max=arr[0],start=0,end=0;

	for(int i=0;i<N;i++){
		sum = sum + arr[i];
		if(sum < 0){
			sum = 0;
			start = i+1;
		}
		if(max < sum){
			max = sum;
			end = i;
		}
	}
	printf("The maximum sum of the sub array is: %d\n",max);
	printf("The ranges of max sub array sum is:\n start:%d\n end:%d\n",start,end);
}

void main(){
	int arr[] ={-2,1,-3,4,-1,2,1,-5,4};
	int size = sizeof(arr)/sizeof(arr[0]);
	Sum(arr,size);
}
