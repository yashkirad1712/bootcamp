/*Sub array sum using Prefix Sum*/

#include<stdio.h>

void SubArrSum(int arr[],int N){
	int psum[N];
	psum[0] = arr[0];
	
	printf(".....Creating Prefix Array......\n");
	for(int i=1;i<N;i++){
		psum[i] = psum[i-1] + arr[i];
	}

	printf("Ready For the Sub Array Operation\n");

	for(int i=0;i<N;i++){
		int sum = 0;
		for(int j=i ;j<N;j++){
			if(i == 0){
				sum = psum[j];
			}else{
				sum = psum[j]-psum[i-1];
			}
			printf("%d\n",sum);
		}
	}

}

void main(){
	int arr[] = {2,4,1,3};
	SubArrSum(arr,4);
}
