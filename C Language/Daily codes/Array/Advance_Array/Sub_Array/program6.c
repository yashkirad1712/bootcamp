/*Maximum Sub Array*/
/*Given An integer array of size n,find the contagious subarray(containing atleast one number)which has the largest sum & return its sum*/

#include<stdio.h>

int MaxSum(int arr[],int N){
	int max = arr[0];

	for(int i=0 ;i<N;i++){
		int sum = 0;
		for(int j=i;j<N;j++){
			sum = sum + arr[j];
			printf("The sum of the Sub Arrays is:%d\n",sum);
			if(max < sum){
				max = sum;
			}
		}
	}
	return max;
}

void main(){
	int arr[] = {-2,1,-3,4,-1,2,1,-5,4};
	int size = sizeof(arr)/sizeof(arr[0]);
	int Max = MaxSum(arr,size);
	printf("The maximum sum of the sub array present is:%d\n",Max);
}
