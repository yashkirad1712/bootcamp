/*Return the count of the number of elements having other elements greater then itself if present*/

#include<stdio.h>
/*Brute force approach*/

/*int count =;
 for(int i=0 ;i<size;i++){
 	for(int j=0 ;j<size ;j++){
		if(arr[i] < arr[j]){
			count++;
			break;
		}
	}
  }
  return count;
*/

/*Optimised Way*/
int GreaterEle(int arr[],int size){
	int max = arr[0];
	int count = 0;
	for(int i=0 ;i<size;i++){
		if(max < arr[i]){
			max = arr[i];
			count = 0;
			i = 0;
		}
		
		if(max == arr[i]){
			count ++;
		}
	}

	return count;
}

void main(){
	printf("Enter the size of array\n");
	int size;
	scanf("%d",&size);
	int arr[size];

	printf("Enter the elements of the array\n");
	for(int i=0 ;i<size; i++){
		scanf("%d",&arr[i]);
	}
	
	printf("The array so formed is\n");
	for(int i=0 ;i<size; i++){
		printf("| %d |",arr[i]);
	}
	
	int ret = GreaterEle(arr,size);
	printf("\nThe count of maximum element is %d\n",ret);
	printf("The count of greater elementrs is %d\n",size - ret);

}
