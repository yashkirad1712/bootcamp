/*Range Sum : find the sum of th eelements given in the range ,where inputs where given by the user as queries*/

#include<stdio.h>

void sum(int arr[],int size){
	int query;
	printf("\nEnter the Number of queries you want\n");
	scanf("%d",&query);
	
	for(int i=0;i<query;i++){
		printf("Enter the start Range\n");
		int start;
		scanf("%d",&start);
		printf("Enter the end Rnage\n");
		int end;
		scanf("%d",&end);
		int sum =0;
		for(int i=start;i<=end;i++){
			if(end > size || start < 0){
				printf("The sum of this range is invalid\n");
				break;
			}
			sum = sum + arr[i];
		}
		printf("The sum of the given range is %d\n ",sum);
	}	
}

void main(){
	int size;
	printf("Enter the size of the array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter the elements of the array\n");
	for(int i=0 ;i<size ;i++){
		scanf("%d",&arr[i]);
	
	}
	
	sum(arr,size);

}
