/*Carry Forward Array*/
/*Here every preceding element is taken forward to check with the suceding element*/

/*LEFTMOST ARRAY*/

#include<stdio.h>

int* leftMost(int arr[],int size,int LMost[]){

	LMost[0] = arr[0];

	for(int i=1 ;i<size;i++){
		if(LMost[i-1] < arr[i]){
			LMost[i] = arr[i];
		}else{
			LMost[i] = LMost[i-1];
		}
	}
	return LMost;
}

void main(){
	int arr[] = {3,5,9,4,1,10,7};
	int size = sizeof(arr)/sizeof(arr[0]);
	int LMost[size];
	int *ptr = leftMost(arr,size,LMost);
	printf("The Left Most Array is\n");
	for(int i=0 ;i<size;i++){
		printf("| %d |",LMost[i]);
	}
	printf("\n");

}
