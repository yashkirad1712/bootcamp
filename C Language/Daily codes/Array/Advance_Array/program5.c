/*Carry Forward Array*/
/*Here every preceding element is taken forward to check with the suceding element*/

/*RIGHTMOST ARRAY*/

#include<stdio.h>

int* RightMost(int arr[],int size,int RMost[]){

	RMost[size-1] = arr[size-1];
	
	for(int i=size-2 ;i>=0;i--){
		if(RMost[i+1] < arr[i]){
			RMost[i] = arr[i];
		}else{
                    RMost[i] = RMost[i+1];   
		}
	}
	return RMost;
}

void main(){
	int arr[] = {3,5,9,4,1,10,7};
	int size = sizeof(arr)/sizeof(arr[0]);
	int RMost[size];
	int *ptr = RightMost(arr,size,RMost);
	printf("The Right Most Array is\n");
	for(int i=0 ;i<size;i++){
		printf("| %d |",RMost[i]);
	}
	printf("\n");

}
