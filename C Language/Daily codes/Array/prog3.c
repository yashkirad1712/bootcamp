/*taking array and printing it with for loop and scanf*/

#include<stdio.h>

void main(){
    int arr[5];

    printf("Enter the elements of the array\n");

    for(int i=0;i<5;i++){
        scanf("%d",&arr[i]);
    }
    printf("The elements of the array are:");
    for(int i=0;i<5;i++){
        printf("%d  ",arr[i]);
    }

}
