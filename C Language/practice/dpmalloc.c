#include<stdio.h>
void* malloc(size_t);

void danglingpointer(int x){
    int *ptr=(int*)malloc(sizeof(int));
    *ptr=x;

    printf("%p\n",ptr);
    printf("%d\n",*ptr);

    int*ptr1=ptr;
    free(ptr);
    printf("%p\n",ptr1);
    printf("%d\n",*ptr1);
}
void main(){
    danglingpointer(20);
}
