/*WAP to take string as a input in the structure*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct node {
    int data;
    char carr[20];
    struct node *next;
};

int main() {
    char lang[20];

    struct node *obj1 = malloc(sizeof(struct node));

    printf("Enter the data in the node: ");
    scanf("%d", &obj1->data);

    printf("Enter the string: ");
    getchar();
    fgets(lang,sizeof(lang),stdin);
    strcpy(obj1->carr, lang);

    obj1->next = NULL;

    printf("%d\n", obj1->data);
    printf("%s\n", obj1->carr);

    free(obj1); // Free dynamically allocated memory

    return 0;
}

