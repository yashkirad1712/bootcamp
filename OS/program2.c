/*Fork and exec call, Parent function will execute after child function in Operating System*/

#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>

int main(){
	int pid = fork();

	if(pid > 1){//parent

		wait(NULL);
		//parent will wait until the child returns the null Value

		printf("Parent = %d\n",getpid());
		printf("Terminal = %d\n",getppid());
	
	}else if(pid == 0){//Child

		printf("Chid = %d\n",getpid());
		printf("Parent = %d\n",getppid());
		
		/*Action to be doen in / constant value / NULL*/
		execlp("/bin/ps","Yash",NULL);
	}else{
		return -1;
	}
}
