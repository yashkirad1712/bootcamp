/*Fork call in Operating System*/

#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>

int main(){
	int pid = fork();

	if(pid > 1){
		printf("Parent = %d\n",getpid());
		printf("Terminal = %d\n",getppid());
	}else if(pid == 0){
		printf("Chid = %d\n",getpid());
		printf("Parent = %d\n",getppid());
	}else{
		return -1;
	}
}
